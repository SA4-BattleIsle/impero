package ch.usi.inf.sa4.impero.entity.test;

import static org.junit.Assert.assertEquals;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import ch.usi.inf.sa4.impero.entity.Ship;
import ch.usi.inf.sa4.impero.game.Player;

public class ShipTest {
	int health = 30;
	int damage = 20;
	Ship ship;
	@Before
	public void setUp() throws Exception {
		Player player = new Player("test", "player",100);
		ship = new Ship(player);
	}

	@After
	public void tearDown() throws Exception {
		ship = null;
	}

	@Test
	public void testGetHealthStatus() {
		assertEquals(health, ship.getHealthStatus());;
	}
	@Test
	public void testGetDamage() {
		assertEquals(damage, ship.damage() );
	}

}
