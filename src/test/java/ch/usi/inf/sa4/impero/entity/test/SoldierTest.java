package ch.usi.inf.sa4.impero.entity.test;

import static org.junit.Assert.assertEquals;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import ch.usi.inf.sa4.impero.entity.Soldier;
import ch.usi.inf.sa4.impero.game.Player;

public class SoldierTest {
	Soldier soldier;
	int health = 10;
	int damage = 5;
	@Before
	public void setUp() throws Exception {
		Player player = new Player("test", "player",100);
		soldier = new Soldier(player);
	}

	@After
	public void tearDown() throws Exception {
		soldier = null;
		
	}

	@Test
	public void testGetHealthStatus() {
		assertEquals(health, soldier.getHealthStatus());;
	}
	@Test
	public void testGetDamage() {
		assertEquals(damage, soldier.damage() );
	}

}
