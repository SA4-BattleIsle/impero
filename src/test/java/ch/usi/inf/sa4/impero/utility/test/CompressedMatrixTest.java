package ch.usi.inf.sa4.impero.utility.test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import ch.usi.inf.sa4.impero.utility.CompressedArray;
import ch.usi.inf.sa4.impero.utility.CompressedMatrix;

public class CompressedMatrixTest {
	
	private CompressedMatrix map;
	private int rows = 67;
	private int columns = 47;

	@Before
	public void setUp() throws Exception {
		
		this.map = new CompressedMatrix();
		CompressedArray column = null;
		//add some data to the column
		for (int i = 0; i < rows; i++) {	
			column = new CompressedArray();
			
			for (int j = 0; j < columns; j++) {
				column.add((int) (Math.random() * 100));
			}
			
			map.add(column);
		}
		
	}

	@After
	public void tearDown() throws Exception {
		
	}

	@Test
	public void testSize() {
		assertEquals(rows, map.size());
	}

	@Test
	public void testIsEmpty() {
		CompressedMatrix test = new CompressedMatrix();
		assertTrue(test.isEmpty());
	}

	@Test
	public void testContains() {
		CompressedArray column = new CompressedArray(columns);
		map.add(column);
		assertTrue(map.contains(column));
	}

	@Test
	public void testRemove() {
		CompressedArray column = new CompressedArray(columns);
		map.add(column);
		assertEquals(rows + 1, map.size());
		map.remove(column);
		assertEquals(rows, map.size());
	}

	@Test
	public void testClear() {
		map.clear();
		assertEquals(0, map.size());
	}

	@Test
	public void testCompressedMatrixIntInt() throws Exception {
		map = new CompressedMatrix(columns, rows);
		assertEquals(columns, map.getNumColumns());
		assertEquals(rows, map.getNumRows());
	}
	
	
}
