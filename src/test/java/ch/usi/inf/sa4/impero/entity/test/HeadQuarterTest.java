package ch.usi.inf.sa4.impero.entity.test;

import static org.junit.Assert.assertEquals;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import ch.usi.inf.sa4.impero.entity.HeadQuarter;
import ch.usi.inf.sa4.impero.game.Player;

public class HeadQuarterTest {
	
	private HeadQuarter hq;
	private Player p;
	private String firstName = "firstName";
	private String lastName = "lastName";

	@Before
	public void setUp() throws Exception {
		p = new Player(firstName, lastName,100);
		this.hq = new HeadQuarter(p);
	}

	@After
	public void tearDown() throws Exception {
	}

	@Test
	public void testGetOwner() throws Exception {
		assertEquals(p, hq.getOwner());
	}

	@Test
	public void testGetHealthStatus() throws Exception {
//		throw new RuntimeException("not yet implemented");
	}

	@Test
	public void testReciveDamage() throws Exception {
//		throw new RuntimeException("not yet implemented");
	}

}
