package ch.usi.inf.sa4.impero.entity.test;

import static org.junit.Assert.assertEquals;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import ch.usi.inf.sa4.impero.entity.Airplane;
import ch.usi.inf.sa4.impero.game.Player;

public class AirplaneTest {
	int health = 30;
	int damage = 20;
	Airplane airplane;
	@Before
	public void setUp() throws Exception {
		Player player = new Player("test", "player",100);
		airplane = new Airplane(player);
	}

	@After
	public void tearDown() throws Exception {
		airplane = null;
	}

	@Test
	public void testGetHealthStatus() {
		assertEquals(health, airplane.getHealthStatus());;
	}
	@Test
	public void testGetDamage() {
		assertEquals(damage, airplane.damage() );
	}
}
