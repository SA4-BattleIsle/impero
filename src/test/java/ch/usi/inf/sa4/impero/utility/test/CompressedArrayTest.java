package ch.usi.inf.sa4.impero.utility.test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import ch.usi.inf.sa4.impero.utility.CompressedArray;

public class CompressedArrayTest {

	private CompressedArray array;
	private int length = 10;
	
	@Before
	public void setUp() throws Exception {
		
		array = new CompressedArray();
		for (int i = 0; i < length; i++) {
			array.add( (int) (Math.random() * 100) );
		}
	}

	@After
	public void tearDown() throws Exception {
		
		array = null;
	}

	@Test
	public void testSize() {
		assertEquals(length, array.size());
	}

	@Test
	public void testIsEmpty() {
		CompressedArray test = new CompressedArray();
		assertEquals(0, test.size());
	}

	@Test
	public void testContains() {
		int num = 5;
		array.add(num);
		assertTrue(array.contains(num));
	}

	@Test
	public void testToArray() {
		Object[] testArray = array.toArray();
		assertEquals(length, testArray.length);
		
		for (int i = 0; i < testArray.length; i++) {
			assertEquals(array.get(i), testArray[i]);
		}
		
	}

	@Test
	public void testRemoveObject() {
		Integer num = new Integer(5);
		array.add(num);
		assertEquals(true, array.remove(num));
		assertEquals(length, array.size());
	}

	@Test
	public void testGet() {
		Integer num = new Integer(5);
		int index = length/2;
		array.add(index, num);
		assertEquals(num, array.get(index));
	}
	
	@Test
	public void testClear() {
		array.clear();
		assertEquals(0, array.size());
	}

}
