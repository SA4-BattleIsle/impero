package ch.usi.inf.sa4.impero.utility.test;

import static org.junit.Assert.assertEquals;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import ch.usi.inf.sa4.impero.utility.ColumnSlice;

public class ColumnSliceTest {
	
	private ColumnSlice slice;
	private int type = 2;
	private int length = 5;

	@Before
	public void setUp() throws Exception {
		
		slice = new ColumnSlice(type, length);
	}

	@After
	public void tearDown() throws Exception {
		slice = null;
	}

	@Test
	public void testGetType() {
		assertEquals(type, slice.getType());
	}

	@Test
	public void testGetLength() {
		assertEquals(length, slice.getLength());
	}

}
