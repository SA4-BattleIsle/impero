package ch.usi.inf.sa4.impero.Action.test;

import static org.junit.Assert.assertEquals;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import ch.usi.inf.sa4.impero.entity.Soldier;
import ch.usi.inf.sa4.impero.entity.Tank;
import ch.usi.inf.sa4.impero.game.Player;

public class AttackActionTest {
	Soldier soldier;
	Tank tank;
	int tankDamagedHealth;
	@Before
	public void setUp() throws Exception {
		Player player = new Player("test", "player",100);
		soldier = new Soldier(player);
		tank = new Tank(player);
		tankDamagedHealth = tank.getHealthStatus() - soldier.damage(); 
	}

	@After
	public void tearDown() throws Exception {
		soldier = null;
		tank = null;
	}

	@Test
	public void test() {
		soldier.attack(tank);
		assertEquals(tankDamagedHealth, tank.getHealthStatus());
	}

}
