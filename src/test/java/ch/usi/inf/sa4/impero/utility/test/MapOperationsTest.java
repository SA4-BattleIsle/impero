package ch.usi.inf.sa4.impero.utility.test;

import org.junit.Before;
import org.junit.Test;

import ch.usi.inf.sa4.impero.entity.Soldier;
import ch.usi.inf.sa4.impero.game.Game;
import ch.usi.inf.sa4.impero.hexMap.Coordinate;
import ch.usi.inf.sa4.impero.hexMap.HexMap;
import ch.usi.inf.sa4.impero.mapGeneration.MapGenerator;
import ch.usi.inf.sa4.impero.turn.Turn;
import ch.usi.inf.sa4.impero.utility.CompressedArray;
import ch.usi.inf.sa4.impero.utility.CompressedMatrix;
import ch.usi.inf.sa4.impero.utility.MapOperations;

public class MapOperationsTest {
	HexMap map;
	
	@Before
	public void startUp() {
		CompressedMatrix compMap = new CompressedMatrix();
		CompressedArray ca1 = new CompressedArray();
		ca1.add(1);
		ca1.add(1);
		ca1.add(1);
		ca1.add(1);
		ca1.add(1);
		ca1.add(1);
		ca1.add(1);
		ca1.add(1);
		ca1.add(1);
		ca1.add(1);
		compMap.add(ca1);
		CompressedArray ca2 = new CompressedArray();
		ca2.add(1);
		ca2.add(2);
		ca2.add(2);
		ca2.add(2);
		ca2.add(2);
		ca2.add(2);
		ca2.add(2);
		ca2.add(2);
		ca2.add(2);
		ca2.add(2);
		ca2.add(1);
		compMap.add(ca2);
		CompressedArray ca3 = new CompressedArray();
		ca3.add(1);
		ca3.add(2);
		ca3.add(2);
		ca3.add(3);
		ca3.add(2);
		ca3.add(3);
		ca3.add(2);
		ca3.add(3);
		ca3.add(2);
		ca3.add(1);
		compMap.add(ca3);
		CompressedArray ca4 = new CompressedArray();
		ca4.add(1);
		ca4.add(2);
		ca4.add(3);
		ca4.add(5);
		ca4.add(5);
		ca4.add(4);
		ca4.add(5);
		ca4.add(3);
		ca4.add(2);
		ca4.add(1);
		compMap.add(ca4);
		CompressedArray ca5 = new CompressedArray();
		ca5.add(1);
		ca5.add(2);
		ca5.add(3);
		ca5.add(3);
		ca5.add(4);
		ca5.add(1);
		ca5.add(4);
		ca5.add(2);
		ca5.add(2);
		ca5.add(1);
		compMap.add(ca5);
		CompressedArray ca6 = new CompressedArray();
		ca6.add(1);
		ca6.add(2);
		ca6.add(2);
		ca6.add(2);
		ca6.add(3);
		ca6.add(3);
		ca6.add(1);
		ca6.add(2);
		ca6.add(2);
		ca6.add(1);
		compMap.add(ca6);
		CompressedArray ca7 = new CompressedArray();
		ca7.add(1);
		ca7.add(1);
		ca7.add(1);
		ca7.add(2);
		ca7.add(3);
		ca7.add(2);
		ca7.add(1);
		ca7.add(2);	
		ca7.add(2);
		ca7.add(1);
		compMap.add(ca7);
		CompressedArray ca8 = new CompressedArray();
		ca8.add(1);
		ca8.add(2);
		ca8.add(2);
		ca8.add(2);
		ca8.add(2);
		ca8.add(2);
		ca8.add(3);
		ca8.add(2);
		ca8.add(2);
		ca8.add(1);
		compMap.add(ca8);
		CompressedArray ca9 = new CompressedArray();
		ca9.add(1);
		ca9.add(1);
		ca9.add(1);
		ca9.add(1);
		ca9.add(2);
		ca9.add(1);
		ca9.add(2);
		ca9.add(1);
		ca9.add(1);
		ca9.add(1);
		compMap.add(ca9);
		CompressedArray ca10 = new CompressedArray();
		ca10.add(1);
		ca10.add(1);
		ca10.add(1);
		ca10.add(1);
		ca10.add(1);
		ca10.add(1);
		ca10.add(1);
		ca10.add(1);
		ca10.add(1);
		ca10.add(1);
		compMap.add(ca10);
		map = new HexMap(compMap);
	}
	
	@Test
	public void testGetPossibleCellDestinations() {
		
		MapGenerator gen = new MapGenerator();
		Game game = new Game(100, gen.generateMap(200, 200, 100, 20, 5, 10, false, false), "test", "test");
		Turn turn = new Turn(200);
	
		//System.out.println(MapOperations.getPossibleCellDestinations(new Coordinate(4,4), game.getMap(), turn, Soldier.class));
		
	}
	
	@Test
	public void testGetShortestPath() {
		//System.out.println(MapOperations.getPossibleCellDestinations(new Coordinate(4,4), map, new Turn(10), Soldier.class).size());
		//System.out.println(MapOperations.getShortestPath(new Coordinate(4, 4), new Coordinate(2, 1), map, new Turn(10),Soldier.class));
	}
}
