package ch.usi.inf.sa4.impero.entity.test;

import static org.junit.Assert.assertEquals;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import ch.usi.inf.sa4.impero.entity.Tank;
import ch.usi.inf.sa4.impero.game.Player;

public class TankTest {
	int health = 20;
	int damage = 100;
	Tank tank;
	@Before
	public void setUp() throws Exception {
		Player player = new Player("test", "player",100);
		tank = new Tank(player);
	}

	@After
	public void tearDown() throws Exception {
		tank= null;
	}

	@Test
	public void testGetHealthStatus() {
		assertEquals(health, tank.getHealthStatus());;
	}
	@Test
	public void testGetDamage() {
		assertEquals(damage, tank.damage() );
	}
}
