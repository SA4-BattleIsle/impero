package ch.usi.inf.sa4.impero.Action.test;

import static org.junit.Assert.assertEquals;

import java.util.Iterator;
import java.util.Map.Entry;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import ch.usi.inf.sa4.impero.entity.Entity;
import ch.usi.inf.sa4.impero.entity.HeadQuarter;
import ch.usi.inf.sa4.impero.game.Game;
import ch.usi.inf.sa4.impero.game.Player;
import ch.usi.inf.sa4.impero.hexMap.Coordinate;
import ch.usi.inf.sa4.impero.hexMap.HexMap;
import ch.usi.inf.sa4.impero.mapGeneration.MapGenerator;

/**
 * Test class for the StartUpAction class. It first create a game and then test if the number of headquarters placed on the map is equal
 * to the number of players. For instance, the headquarters are placed randomly, but in future it will also test if the headquarters are placed in the right position.
 *
 * @author Luca
 *
 */
public class StartUpActionTest {
	//game object used for tests
	private Game game;
	
	/**
	 * Create an instance of class Game.
	 * 
	 * @throws Exception
	 */
	@Before
	public void setUp() throws Exception {
		MapGenerator mapGenerator = new MapGenerator();
		this.game = new Game(100, mapGenerator.generateMap(20, 20, 70, 20, 5, 10, false, false), "Luca", "Dotti");
	}

	@After
	public void tearDown() throws Exception {
	}
	
	/**
	 * Set a new game with dimensions according to the number of players.
	 * 
	 * @param nbPlayers
	 */
	private void setNewGame(int nbPlayers) {
		MapGenerator mapGenerator = new MapGenerator();
		this.game = new Game(100, mapGenerator.generateMap(20*nbPlayers, 20*nbPlayers, 70, 20, 5, 10, false, false), "Luca", "Dotti");
	}
	
	/**
	 * Test the number of headquarters according to the number of players.
	 */
	@Test
	public void testNumberOfHeadQuarters() {
		
		for (int i = 0; i < 10; i++) {
			game.addPlayer(new Player("Player" + i, "Player "+ i,100));
			
			/*
			//System.out.println(countHeadQuartersOnMap(game.getMap()));
			if(i != 0) {
				this.setNewGame(i+1);
				for (int j = 0; j < i; j++) {
					game.addPlayer(new Player("a","b"));
				}
				Action a = new StartUpAction();
				a.execute(game);
				assertEquals(i+1, countHeadQuartersOnMap(game.getMap()));
			} else {
				Action a = new StartUpAction();
				a.execute(game);
				
				assertEquals(i+1, countHeadQuartersOnMap(game.getMap()));
			}
			*/
		}
		game.start();
		System.out.println(countHeadQuartersOnMap(game.getMap()));
		assertEquals(11, countHeadQuartersOnMap(game.getMap()));
	}
	
	/**
	 * Method to count the number of headquarters on the map.
	 * @param map
	 * @return
	 */
	private int countHeadQuartersOnMap(HexMap map) {
		java.util.Map<Coordinate,Entity> entities = map.getAllEntities();
		Iterator<Entry<Coordinate, Entity>> it = entities.entrySet().iterator();
		int count = 0;
	    while (it.hasNext()) {
	        java.util.Map.Entry<Coordinate, Entity> pairs = (java.util.Map.Entry<Coordinate, Entity>)it.next();
	        if(pairs.getValue() instanceof HeadQuarter) {
	        	count++;
	        }
	    }
	    return count;
	}
}
