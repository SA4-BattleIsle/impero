package ch.usi.inf.sa4.impero.Action.test;

import static org.junit.Assert.assertEquals;

import java.util.Random;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import ch.usi.inf.sa4.impero.action.user.BuildAction;
import ch.usi.inf.sa4.impero.entity.HeadQuarter;
import ch.usi.inf.sa4.impero.game.Game;
import ch.usi.inf.sa4.impero.game.Player;
import ch.usi.inf.sa4.impero.hexMap.Coordinate;
import ch.usi.inf.sa4.impero.hexMap.terrain.TerrainType;
import ch.usi.inf.sa4.impero.mapGeneration.MapGenerator;

/**
 * Test class for the BuildAction class. For the moment it only tests if at the chosen position there actually is the correct entity. 
 * Every test method will test ten times all the implemented buildings.
 * 
 * @author Luca
 *
 */
public class BuildActionTest {
	//Game object used to make tests
	Game game;
	
	/**
	 * Create an instance of Game and a random map from the MapGenerator object.
	 * @throws Exception
	 */
	@Before
	public void setUp() throws Exception {
		MapGenerator mapGenerator = new MapGenerator();
		this.game = new Game(100, mapGenerator.generateMap(200, 200, 100, 20, 5, 10, false, false), "Luca", "Dotti");
	}

	@After
	public void tearDown() throws Exception {
	}
	
	/**
	 * Test ten headquarters placed randomly on a ground cell. It first find the coordinates, create a build action, add it to the game and
	 * execute it. Finally it test if there if there is an entity on the chosen coordinates and then tests if the entity on it is of type
	 * HeadQuarters.
	 * 
	 */
	@Test
	public void testHeadQuarterPosition() {
		Random rand = new Random();
		Player testp = new Player("Test","Test",100);
		game.addPlayer(testp);
		game.start();

		for (int i = 0; i < 10; i++) {
			int x = rand.nextInt(game.getMap().getWidth());
			int y = rand.nextInt(game.getMap().getHeight());
			Coordinate coordinate = new Coordinate(x, y);
			while(!game.getMap().checkTerrainType(coordinate, TerrainType.SAND) && game.getMap().coordinateHasEntity(coordinate)) {
				x = rand.nextInt(game.getMap().getWidth());
				y = rand.nextInt(game.getMap().getHeight());
			}
			testp.addAction(new BuildAction(coordinate, HeadQuarter.class, testp));
			game.execute();
			assertEquals(true, game.getMap().coordinateHasEntity(coordinate));
			assertEquals(HeadQuarter.class, game.getMap().getEntityByCoordinates(coordinate).getClass());

		}
	}
	
	/**
	 * Test ten energy extraction facility placed randomly on a ground cell. It first find the coordinates, create a build action, add it to the
	 * game and execute it. Finally it test if there if there is an entity on the chosen coordinates and then tests if the entity on it is of type
	 * EnergyExtractionFacility.
	 */
	/*
	@Test
	public void testEnergyExtractorFacilityPosition() {
		
		Random rand = new Random();
		Player testp = new Player("Test","Test");
		for (int i = 0; i < 10; i++) {
			int x = rand.nextInt(game.getMap().getWidth());
			int y = rand.nextInt(game.getMap().getHeight());
			Coordinate coordinate = new Coordinate(x, y);
			while(!game.getMap().checkTerrainType(coordinate, TerrainType.ENERGYRESOURCE) && game.getMap().coordinateHasEntity(coordinate)) {
				x = rand.nextInt(game.getMap().getWidth());
				y = rand.nextInt(game.getMap().getHeight());
			}
			game.addPlayer(testp);
			game.start();
			testp.addAction(new BuildAction(coordinate, HeadQuarter.class, testp));
			game.execute();
			assertEquals(true, game.getMap().coordinateHasEntity(coordinate));
			assertEquals(EnergyExtractionFacility.class, game.getMap().getEntityByCoordinates(coordinate).getClass());
		}
	}*/
}
