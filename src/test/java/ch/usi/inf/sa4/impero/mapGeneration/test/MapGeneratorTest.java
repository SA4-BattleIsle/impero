package ch.usi.inf.sa4.impero.mapGeneration.test;

import static org.junit.Assert.assertEquals;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import ch.usi.inf.sa4.impero.hexMap.HexMap;
import ch.usi.inf.sa4.impero.mapGeneration.MapGenerator;
import ch.usi.inf.sa4.impero.utility.CompressedMatrix;

public class MapGeneratorTest {
	
	private MapGenerator mg;
	
	private final int width = 100;
	private final int height = 75;
	private final int landPercent = 50;
	private final int waterPercent = 10;
	private final int mountainPercent = 10;
	private final int energyPercent = 3;
	private final boolean lakes = false;
	private final boolean rivers = false;

	@Before
	public void setUp() throws Exception {
		mg = new MapGenerator();
	}

	@After
	public void tearDown() throws Exception {
	}

	@Test
	public void testGenerateMap() throws Exception {
		HexMap hexMap = mg.generateMap(
				width, height, landPercent, waterPercent, 
				mountainPercent, energyPercent, lakes, rivers);
		
		CompressedMatrix mapMatrix = hexMap.getMap();
		
		assertEquals(width, mapMatrix.getNumColumns());
		assertEquals(height, mapMatrix.getNumRows());
	}

	@Test
	public void testGenerateNoise() throws Exception {
		// TODO
//		throw new RuntimeException("not yet implemented");
	}

	@Test
	public void testGenerateRiver() throws Exception {
		// TODO
//		throw new RuntimeException("not yet implemented");
	}

}
