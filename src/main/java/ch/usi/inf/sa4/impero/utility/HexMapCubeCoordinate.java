package ch.usi.inf.sa4.impero.utility;

import ch.usi.inf.sa4.impero.hexMap.Coordinate;

/**
 * @author jesper
 *
 */
public class HexMapCubeCoordinate {
	
	private int x;
	private int y;
	private int z;
	
	/**
	 * @param x
	 * @param y
	 * @param z
	 */
	public HexMapCubeCoordinate(int x, int y, int z) {
		this.x = x;
		this.y = y;
		this.z = z;
	}
	
	/**
	 * Create a cube coordinate from a even-q coordinate
	 * 
	 * @param evenQCoordinate	the coordinates of a cell in even-q
	 */
	public HexMapCubeCoordinate(Coordinate evenQCoordinate) {
		this(evenQCoordinate.getX(), evenQCoordinate.getY());	
	}
	
	/**
	 * Create a cube coordinate from even-q coordinates
	 * 
	 * @param xEvenQ	x coordinate even-q
	 * @param yEvenQ	y-coordinate even-q
	 */
	public HexMapCubeCoordinate(int xEvenQ, int yEvenQ) {
		
		// convert even-q offset to cube
		this.x = xEvenQ;
		this.z = yEvenQ - (xEvenQ + (xEvenQ & 1)) / 2;
		this.y = - x - z;
	}

	/**
	 * @return the x
	 */
	public int getX() {
		return x;
	}

	/**
	 * @param x the x to set
	 */
	public void setX(int x) {
		this.x = x;
	}

	/**
	 * @return the y
	 */
	public int getY() {
		return y;
	}

	/**
	 * @param y the y to set
	 */
	public void setY(int y) {
		this.y = y;
	}

	/**
	 * @return the z
	 */
	public int getZ() {
		return z;
	}

	/**
	 * @param z the z to set
	 */
	public void setZ(int z) {
		this.z = z;
	}
	
	public Coordinate getEvenQCoordinate() {
		
		//convert cube to even-q offset
		int xEvenQ = x;
		int yEvenQ = z + (x + (x & 1)) / 2;
		
		return new Coordinate(xEvenQ, yEvenQ);
	}
	
	

}
