package ch.usi.inf.sa4.impero.entity;

import java.awt.Color;

import ch.usi.inf.sa4.impero.game.Player;

/**
 * @author Talal
 * 
 */

public class Soldier extends AbstractGroundUnit {
	
	public Soldier(final Player owner) {
		super(owner);
		super.health = 10;
		super.damage = 5;
		super.name = "Soldier";
		// dark olive green
		this.color = new Color(85,107,47);
	}
}
