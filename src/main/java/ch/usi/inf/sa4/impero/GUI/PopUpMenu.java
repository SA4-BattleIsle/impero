package ch.usi.inf.sa4.impero.GUI;

import java.util.List;

import javax.swing.JMenuItem;
import javax.swing.JPopupMenu;

import ch.usi.inf.sa4.impero.entity.Entity;
import ch.usi.inf.sa4.impero.game.Game;
import ch.usi.inf.sa4.impero.hexMap.Coordinate;
import ch.usi.inf.sa4.impero.hexMap.terrain.TerrainType;

class PopUpMenu extends JPopupMenu {
	
    public PopUpMenu(List<JMenuItem> items){
    	for(JMenuItem i: items){
    		this.add(i);
    	}
    }
    
    public PopUpMenu(JMenuItem i){
    	this.add(i);
    }

    /**
     * Creates a contextual menu based on entity that is on the cell
     * @param entity
     */
	public PopUpMenu(Entity entity, Game game, Coordinate cell) {
		MenuItemMapperUnit mim = new MenuItemMapperUnit(game,cell);
		List<JMenuItem> items = mim.getMenuOptions(entity.getClass());
		for(JMenuItem m:items){
			this.add(m);
		}
	}

	/**
	 * Creates a contextual menu based on the terraintype
	 * @param terrain
	 */
	public PopUpMenu(TerrainType terrain, Game game, Coordinate cell) {
		MenuItemMapperBuilding mim = new MenuItemMapperBuilding(game,cell);
		List<JMenuItem> items = mim.getMenuOptions(terrain);
		for(JMenuItem m:items){
			this.add(m);
		}
	}
}
