package ch.usi.inf.sa4.impero.entity;

import java.awt.Polygon;

import ch.usi.inf.sa4.impero.game.Player;

/**
 * @author Talal
 *
 */


public abstract class AbstractUnit extends AbstractEntity {
	
	public AbstractUnit(Player owner) {
		super(owner);
		
	}
	protected int damage;
	
	/**
	 * Send damage to the enemy unit.
	 * @param Enemy unit
	 */
	public void attack(Entity entity){
		 entity.reciveDamage(this.damage) ;
	 }
	public int damage(){
		return this.damage;
	}
	
	/* (non-Javadoc)
	 * @see ch.usi.inf.sa4.impero.entity.Entity#generateShape()
	 */
	public void generateShape() {
		int[] xpoints = {this.x, this.width + this.x, this.width/2 + this.x};
		int[] ypoints = {(int) (this.width * 0.75 + this.y), (int) (this.width * 0.75 + this.y), (int) (Math.sin(60) * this.width) + this.y + this.width/4};
		
		this.shape = new Polygon(xpoints, ypoints, 3);
	}
	
}
