package ch.usi.inf.sa4.impero.hexMap;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import ch.usi.inf.sa4.impero.action.cost.MovementCostMapper;
import ch.usi.inf.sa4.impero.entity.AbstractUnit;
import ch.usi.inf.sa4.impero.entity.Entity;
import ch.usi.inf.sa4.impero.game.Player;
import ch.usi.inf.sa4.impero.hexMap.terrain.TerrainType;
import ch.usi.inf.sa4.impero.turn.Turn;
import ch.usi.inf.sa4.impero.utility.CompressedMatrix;
import ch.usi.inf.sa4.impero.utility.MapOperations;

/**
 * 
 * @author Luca, Talal, Kasim, Jesper
 */
public class HexMap {
	final private CompressedMatrix hexMap;
	final private int width;
	final private int height;
	final private Map<Coordinate, Entity> entities = new HashMap<>();
	
	public HexMap(final CompressedMatrix newHexMap) {

		this.hexMap = newHexMap;
		this.width = newHexMap.size();
		this.height = newHexMap.get(0).size();
	}
	
	/** 
	 * Check if in the given coordinate there is an entity.
	 * 
	 * @param position
	 * @return true if there is an entity on the given coordinate, false otherwise
	 */
	public boolean coordinateHasEntity(final Coordinate position) {
		return this.entities.containsKey(position);
	}
	

	/** 
	 * Get the map of the game
	 * @return
	 */
	public CompressedMatrix getMap() {
		return this.hexMap;
	}
	
	
	/**
	 * Get the entities in the map
	 * @return
	 */
	public java.util.Map<Coordinate, Entity> getAllEntities() {
		return this.entities;
	}
	/**
	 * Returns an Entity by passing the coordinates .
	 * @param  : Coordinates
	 * @return Entity in this coordinates.
	 */
	
	public Entity getEntityByCoordinates(Coordinate coordinates){
		return this.entities.get(coordinates);
	}
	/**
	 * Returns an Coordinate by passing the coordinates .
	 * @param  : Entity
	 * @return Coordinate of the entiy.
	 */
	public Coordinate getCoordinatesByEntity( Entity entity){
		for(Coordinate c : entities.keySet()){
			if(entities.get(c).equals(entity)){
				return(c);
			}
		}
		return null;		
	}
	
	
	/**
	 * Returns a list of entities of a given class
	 */
	public List<Entity> getEntitiesByClass(Class<?> c){
		List<Entity> l = new ArrayList<Entity>();
		for(Entity e :  this.entities.values()){
			if(e.getClass() == c)
				l.add(e);
		}
		return l;
	}

	/**
	 * Returns a list of the player entities .
	 * @param  : the player
	 * @return  ArrayList of entities.
	 */
	public List<Entity> getArmySummary(Player player){
		List<Entity> playerEntityList = new ArrayList<>();		
		for (Entity entity : this.entities.values() ){
			if (entity.getOwner().equals(player) ){
				playerEntityList.add(entity);
			}
		}
		return(playerEntityList);
	}
	
	/**
	 * Returns information about terrain.
	 * @param coordinate : coordinates of the point informations are needed
	 * @return type of terrain
	 */
	public TerrainType getTerrainType(Coordinate coordinate){

		int x = coordinate.getX();
		int y = coordinate.getY();
		
		if ( this.hexMap.get(x) != null && this.hexMap.get(y).get(x) != null) {
			return this.hexMap.getTerrainTypeByCoordinate(coordinate.getX(), coordinate.getY());
			
		}
		
		return null;

		
	}
	
	/**
	 * Getter for the width of the map.
	 * 
	 * @return
	 */
	public int getWidth() {
		return this.hexMap.get(0).size();	
	}

	/**
	 * Getter for the height of the map.
	 * @return height of terrain
	 */
	public int getHeight() {
		return this.hexMap.size();
	}

	/**
	 * Add a given entity at the given coordinates.
	 * 
	 * @param where
	 * @param what
	 */
	public void addEntity(Coordinate where, Entity what) {
		this.entities.put(where, what);
	}
	
	/**
	 * This method 
	 * @param coordinates
	 * @param game
	 * @return
	 */
	private Coordinate findFreeCoordinate(List<Coordinate> coordinates) {
		for (Coordinate coordinate : coordinates) {
			if(!coordinateHasEntity(coordinate)) {
				return coordinate;
			}
		}
		return null;
	}
	
	/**
	 * Find a free cell around the given coordinate.
	 * 
	 * @param coordinate
	 * @return 
	 */
	public Coordinate findFreeCoordinateAround(Coordinate coordinate, Class<? extends AbstractUnit> c) {
		Coordinate spawnCoordinate;
		int range = 1;
		List<Coordinate> neighbors = MapOperations.getNeighbors(coordinate, this.width, this.height);
		
		spawnCoordinate = this.findFreeCoordinate(neighbors);
		while(spawnCoordinate == null || MovementCostMapper.getActionPointCost(c, this.getTerrainType(spawnCoordinate)) < 1) {
			range++;
			if(range > Math.min(width, height)) {
				return null;
			}
			neighbors = MapOperations.getNeighborsInRange(coordinate, range, this.width, this.height);
			spawnCoordinate = this.findFreeCoordinate(neighbors);
		}
		return spawnCoordinate;
	}
	
	/**
	 * Ciao
	 * @param c
	 * @param type
	 * @return
	 */
	public boolean checkTerrainType(Coordinate c, TerrainType type) {
		if(type == hexMap.getByCoordinate(c)) {
			return true;
		} else {
			return false;
		}
		
	}
	
	/**
	 * 
	 * @param from
	 * @param to
	 */
	public void moveEntity(Coordinate from, Coordinate to) {
		Entity entity = entities.get(from);
		entities.remove(from);
		entities.put(to, entity);
	}

	/**
	 * Get the cells where a unit can move
	 * 
	 * @param c		coordinate of the unit
	 * @param turn	turn of the player
	 * @param unit	the unit that wants to move
	 * @return		a list with possible cell coordinates
	 */
	public List<Coordinate> getPossibleCellDestinations(Coordinate c, Turn turn, Class<? extends AbstractUnit> unit) {
		int actionPoints = turn.getActionPoints();
		
		Set<Coordinate> foundCoordinates = new HashSet<Coordinate>();
		Set<Coordinate> visitedCoordinates = new HashSet<Coordinate>();
		
		
		//return MapOperations.getPossibleCoordinate(c, this, actionPoints, unit);
		//return MapOperations.getNeighborsInRange(c, 3, this.getWidth(), this.getHeight());
		//return MapOperations.getNeighbors(c, this.getWidth(), this.getHeight());
		return new ArrayList<Coordinate>(MapOperations.traverse(c, visitedCoordinates, foundCoordinates, actionPoints, this, unit));
	}
}
