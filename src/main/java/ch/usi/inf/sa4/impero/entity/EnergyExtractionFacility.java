package ch.usi.inf.sa4.impero.entity;

import java.awt.Color;

import ch.usi.inf.sa4.impero.game.Player;

/**
 * @author Kasim
 *
 */

public class EnergyExtractionFacility extends AbstractBuilding {

	public EnergyExtractionFacility(Player owner) {
		super(owner);
		super.health = 100;
		super.name = "Energy Extraction Facility";
		
		// antique white
		this.color = new Color(250,235,215);
	}

	public int getBonus() {
		return 10;
	}

}
