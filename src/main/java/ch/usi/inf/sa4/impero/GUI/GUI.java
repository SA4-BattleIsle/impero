package ch.usi.inf.sa4.impero.GUI;

import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;

import ch.usi.inf.sa4.impero.System;
import ch.usi.inf.sa4.impero.game.Game;

/**
 * 
 * @author Jesper
 *
 */
public final class GUI{
	public GUI(){
	}
	
	private static int mapWidth = 70;
	private static int mapHeight = 50;
	private static int landPercent = 50;
	private static int waterPercent;
	private static int mountainPercent = 10;
	private static int energyPercent = 3;
	private static boolean lakes;
	private static boolean rivers;
	private static int actionPointsPerTurn = 20;
	private static JFrame frame;
	private static System system = new System();
	private static Game game;
	
	private static String firstName = "Notch";
	private static String lastName = "Persson";

	public void createAndShowGUI() {
		//Create and set up the window.
        frame = new JFrame("Impero!");
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

        //Create and set up the content pane.
        JPanel contentPane = new JPanel();
        contentPane.setOpaque(true); //content panes must be opaque
        contentPane.setBorder(BorderFactory.createEmptyBorder(10, 10, 10, 10));
        frame.setContentPane(contentPane);
        
     // adding panel for input
        GridBagLayout gridbag = new GridBagLayout();
        GridBagConstraints gridbagConstraints = new GridBagConstraints();
        gridbagConstraints.fill = GridBagConstraints.BOTH;
        JPanel inputPanel = new JPanel(gridbag);
        contentPane.add(inputPanel);
        
        // adding first name input
        final JLabel firstName = new JLabel("First Name");
        gridbagConstraints.gridwidth = GridBagConstraints.BOTH;
        gridbagConstraints.insets = new Insets(0, 0, 5, 0);
        gridbag.setConstraints(firstName, gridbagConstraints);
        inputPanel.add(firstName);
        
        final JTextField firstNameInput = new JTextField(this.firstName);
        gridbagConstraints.gridwidth = GridBagConstraints.REMAINDER;
        gridbag.setConstraints(firstNameInput, gridbagConstraints);
        inputPanel.add(firstNameInput);
        
        // adding last name input
        final JLabel lastName = new JLabel("Last Name");
        gridbagConstraints.gridwidth = GridBagConstraints.BOTH;
        gridbagConstraints.insets = new Insets(0, 0, 5, 0);
        gridbag.setConstraints(lastName, gridbagConstraints);
        inputPanel.add(lastName);
        
        final JTextField lastNameInput = new JTextField(this.lastName);
        gridbagConstraints.gridwidth = GridBagConstraints.REMAINDER;
        gridbag.setConstraints(lastNameInput, gridbagConstraints);
        inputPanel.add(lastNameInput);
        
        // adding some text
        JLabel description = new JLabel("Enter the map parameters below");
        gridbagConstraints.gridwidth = GridBagConstraints.REMAINDER;
        gridbagConstraints.insets = new Insets(5, 0, 10, 0);
        gridbag.setConstraints(description, gridbagConstraints);
        inputPanel.add(description);
        
        // adding width input
        JLabel widthLabel = new JLabel("Width");
        gridbagConstraints.gridwidth = GridBagConstraints.BOTH;
        gridbagConstraints.insets = new Insets(0, 0, 5, 0);
        gridbag.setConstraints(widthLabel, gridbagConstraints);
        inputPanel.add(widthLabel);
        
        final JTextField widthInput = new JTextField("" + mapWidth);
        gridbagConstraints.gridwidth = GridBagConstraints.REMAINDER;
        gridbag.setConstraints(widthInput, gridbagConstraints);
        inputPanel.add(widthInput);

        // adding height input
        JLabel heightLabel = new JLabel("Height");
        gridbagConstraints.gridwidth = GridBagConstraints.BOTH;
        gridbag.setConstraints(heightLabel, gridbagConstraints);
        inputPanel.add(heightLabel);
       
        final JTextField heightInput = new JTextField("" + mapHeight);
        gridbagConstraints.gridwidth = GridBagConstraints.REMAINDER;
        gridbag.setConstraints(heightInput, gridbagConstraints);
        inputPanel.add(heightInput);
        
        // adding land percent input
        final JLabel landPercentLabel = new JLabel("% Land");
        gridbagConstraints.gridwidth = GridBagConstraints.BOTH;
        gridbag.setConstraints(landPercentLabel, gridbagConstraints);
        inputPanel.add(landPercentLabel);
        
        final JTextField landPercentInput = new JTextField("" + landPercent);
        gridbagConstraints.gridwidth = GridBagConstraints.REMAINDER;
        gridbag.setConstraints(landPercentInput, gridbagConstraints);
        inputPanel.add(landPercentInput);
        
        // adding lake and river input
        final JCheckBox lakeCheckBox = new JCheckBox("Lakes");
        gridbagConstraints.gridwidth = GridBagConstraints.BOTH;
        gridbag.setConstraints(lakeCheckBox, gridbagConstraints);
        inputPanel.add(lakeCheckBox);
        
        final JCheckBox riverCheckBox = new JCheckBox("Rivers");
        gridbagConstraints.gridwidth = GridBagConstraints.REMAINDER;
        gridbag.setConstraints(riverCheckBox, gridbagConstraints);
        inputPanel.add(riverCheckBox);
        
        // adding water % input
        JLabel waterPercentLabel = new JLabel("% water");
        gridbagConstraints.gridwidth = GridBagConstraints.BOTH;
        gridbag.setConstraints(waterPercentLabel, gridbagConstraints);
        inputPanel.add(waterPercentLabel);
        
        final JTextField waterPercentInput = new JTextField("" + waterPercent);
        gridbagConstraints.gridwidth = GridBagConstraints.REMAINDER;
        gridbag.setConstraints(waterPercentInput, gridbagConstraints);
        inputPanel.add(waterPercentInput);
        
        // adding mountain % input
        JLabel mountainPercentLabel = new JLabel("% mountain");
        gridbagConstraints.gridwidth = GridBagConstraints.BOTH;
        gridbag.setConstraints(mountainPercentLabel, gridbagConstraints);
        inputPanel.add(mountainPercentLabel);
        
        final JTextField mountainPercentInput = new JTextField("" + mountainPercent);
        gridbagConstraints.gridwidth = GridBagConstraints.REMAINDER;
        gridbag.setConstraints(mountainPercentInput, gridbagConstraints);
        inputPanel.add(mountainPercentInput);
        
        // adding energy % input
        JLabel energyPercentLabel = new JLabel("% energy sources");
        gridbagConstraints.gridwidth = GridBagConstraints.BOTH;
        gridbag.setConstraints(energyPercentLabel, gridbagConstraints);
        inputPanel.add(energyPercentLabel);
        
        final JTextField energyPercentInput = new JTextField("" + energyPercent);
        gridbagConstraints.gridwidth = GridBagConstraints.REMAINDER;
        gridbag.setConstraints(energyPercentInput, gridbagConstraints);
        inputPanel.add(energyPercentInput);
        
        // adding terrain height input
        JLabel terrainHeightLabel = new JLabel("Heights of terrains");
        gridbagConstraints.gridwidth = GridBagConstraints.BOTH;
        gridbag.setConstraints(terrainHeightLabel, gridbagConstraints);
        inputPanel.add(terrainHeightLabel);
        
        JTextField terrainHeightInput = new JTextField("");
        terrainHeightInput.setEnabled(false);
        gridbagConstraints.gridwidth = GridBagConstraints.REMAINDER;
        gridbag.setConstraints(terrainHeightInput, gridbagConstraints);
        inputPanel.add(terrainHeightInput);
        
     // adding # action points input
        JLabel actionPointsLabel = new JLabel("# action points per turn");
        gridbagConstraints.gridwidth = GridBagConstraints.BOTH;
        gridbag.setConstraints(actionPointsLabel, gridbagConstraints);
        inputPanel.add(actionPointsLabel);
        
        final JTextField actionPointsInput = new JTextField("" + actionPointsPerTurn);
        gridbagConstraints.gridwidth = GridBagConstraints.REMAINDER;
        gridbag.setConstraints(actionPointsInput, gridbagConstraints);
        inputPanel.add(actionPointsInput);
        
        // adding start game button
        JButton startGameButton = new JButton("Load Game");
        gridbagConstraints.insets = new Insets(5, 0, 0, 0);
        gridbag.setConstraints(startGameButton, gridbagConstraints);
        inputPanel.add(startGameButton);
        
        // start game button action
        startGameButton.addActionListener(new ActionListener() {

			/* (non-Javadoc)
			 * @see java.awt.event.ActionListener#actionPerformed(java.awt.event.ActionEvent)
			 */
			/* (non-Javadoc)
			 * @see java.awt.event.ActionListener#actionPerformed(java.awt.event.ActionEvent)
			 */
			@Override
			public void actionPerformed(ActionEvent e) {
				
				final String firstName = firstNameInput.getText();
				final String lastName = lastNameInput.getText();
				mapWidth = Integer.parseInt(widthInput.getText());
				mapHeight = Integer.parseInt(heightInput.getText());
				landPercent = Integer.parseInt(landPercentInput.getText());
				waterPercent = Integer.parseInt(waterPercentInput.getText());
				mountainPercent = Integer.parseInt(mountainPercentInput.getText());
				energyPercent = Integer.parseInt(energyPercentInput.getText());
				lakes = lakeCheckBox.isSelected();
				rivers = riverCheckBox.isSelected();
				actionPointsPerTurn = Integer.parseInt(actionPointsInput.getText());
				
				// start the system
				game = system.startGame(firstName, lastName, 
						mapWidth, mapHeight, landPercent, waterPercent, mountainPercent, energyPercent, lakes, rivers, 
						actionPointsPerTurn);
				
				// adds itself to the game observers
				//game.addObserver(GUI.this);
				
				// dispose the current frame and create a new for the map
				frame.dispose();
				frame = new JFrame("Impero!");
		        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
				
				//Create and set up the game panel
		        JPanel gamePanel = new GamePanel(game);
		        gamePanel.setOpaque(true); //content panes must be opaque
		        gamePanel.setBorder(BorderFactory.createEmptyBorder(10, 10, 10, 10));
		        frame.setContentPane(gamePanel);
		        
		        frame.pack();
		        frame.setLocationRelativeTo(null);
		        frame.setVisible(true);
				
			}
		});
        

        //Display the window.
        frame.pack();
        frame.setLocationRelativeTo(null);
        frame.setVisible(true);
	}
	
		
}
