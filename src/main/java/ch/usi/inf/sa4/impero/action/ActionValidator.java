package ch.usi.inf.sa4.impero.action;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import ch.usi.inf.sa4.impero.action.user.AttackAction;
import ch.usi.inf.sa4.impero.action.user.BuildAction;
import ch.usi.inf.sa4.impero.action.user.MoveAction;
import ch.usi.inf.sa4.impero.game.Game;
import ch.usi.inf.sa4.impero.game.Player;
import ch.usi.inf.sa4.impero.hexMap.Coordinate;
import ch.usi.inf.sa4.impero.hexMap.HexMap;

/**
 * @author Kasim
 *
 * validates the sequence of actions and removes invalid actions
 */

public class ActionValidator {
	
	private enum OverrideAction{
		FREE,OCCUPIED
	}
	
	private HexMap map;
	private List<ActionSequence> sequences;
	private Map<Coordinate,OverrideAction> overrides = new HashMap<Coordinate,OverrideAction>();
	private Game game;
	
	
	public ActionValidator(List<ActionSequence> sequences, HexMap map, Game game){
		this.map = map;
		this.sequences = sequences;
		this.game = game;
	}
	
	public List<ActionSequence> validate(){
		Action previous;
		Action current = null;
		boolean first = true;
		
		for(ActionSequence as : this.sequences){
			//Validates each action against the previous ones
			for(Action a : as.getActions()){
				if(first){
					current = a;
					first=false;
				}else{
					previous = current;
					current = a;
					
					//Save comparee behaviour
					this.simulate(previous);
					
				}
				//Check for conflicts
				 if (!this.validateActionAgainstTheSequence(current))
					 {
					 as.removeAction(a);
				 	 game.logEvent(a.toString() +" has been removed due to validation.");
					 }

			}
		}
		return this.sequences;
	}

	/** 
	 * Validates the action against the entire the sequence of actions
	 * @param current
	 * @return true if validated, false if not
	 */
	private boolean validateActionAgainstTheSequence(Action current) {
		System.out.println("Validating "+ current.toString());
		if(current.getClass() == MoveAction.class){
			MoveAction comparatormove = (MoveAction)current; 			
			//Check if source is still there and owner equals action owner
			if(this.overrides.get(comparatormove.getFrom()) == OverrideAction.OCCUPIED ^
					this.map.coordinateHasEntity(comparatormove.getFrom()))
			{
				//Check if destination is occupied
				if(this.overrides.get(comparatormove.getTo()) == OverrideAction.OCCUPIED ^
						this.map.coordinateHasEntity(comparatormove.getTo())){
					//Check if entity owner different from Action owner starts an attack
					if(	this.map.getEntityByCoordinates(comparatormove.getTo()).getOwner() !=
						this.map.getEntityByCoordinates(comparatormove.getFrom()).getOwner() )
						{
						// Attack!
						System.out.println("This is an attack!");
						Coordinate from = ((MoveAction)current).getFrom();
						Coordinate to = ((MoveAction)current).getTo();
						Player player = ((MoveAction)current).getOwner();
						current = new AttackAction(from,to,player);
						current.setEnabled(true);
						return true;
						}
						else
						{
						// Moving to a cell occupied by yourself... epic fail
						current.setEnabled(false);
						return false;
						}
				}else{
					current.setEnabled(true);
					return true;
				}
			}else{
				//Own unit is gone
				System.out.println("Unit is gone!");
				current.setEnabled(false);
				return false;
			}
		}
	
		if(current.getClass() == AttackAction.class){
			//Check if the attacked stuff is still there
			if(this.overrides.get(((AttackAction)current).getAttackee()) == OverrideAction.OCCUPIED ^
					this.map.coordinateHasEntity(((AttackAction)current).getAttackee())){
				current.setEnabled(true);
			}else{
				current.setEnabled(false);
			}
		}
		
		if(current.getClass() == BuildAction.class){
			//Check if the destination cell is still free
			if(this.overrides.get(((BuildAction)current).getDestination()) == OverrideAction.FREE ^
					!this.map.coordinateHasEntity(((BuildAction)current).getDestination())){
				current.setEnabled(true);
			}else{
				current.setEnabled(false);
			}
		}
		return current.getEnabled();
	}

	/**
	 * Simulate action behaviour by saving actual changes to the map
	 * @param comparee
	 */
	private void simulate(Action comparee) {
		if(comparee.getClass() == MoveAction.class){
			//Override old position with free entity
			this.overrides.put(((MoveAction)comparee).getFrom(),OverrideAction.FREE);
			//Override new position with entity
			this.overrides.put(((MoveAction)comparee).getTo(),OverrideAction.OCCUPIED);
		}

		if(comparee.getClass() == BuildAction.class){
			//Set the builded cell occupied
			this.overrides.put(((BuildAction)comparee).getDestination(), OverrideAction.OCCUPIED);
		}
	}
	
}
