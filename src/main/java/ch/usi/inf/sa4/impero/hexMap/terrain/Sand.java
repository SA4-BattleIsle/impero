package ch.usi.inf.sa4.impero.hexMap.terrain;

import java.awt.Color;

/**
 * @author Kasim and Jesper
 *
 */
public class Sand extends AbstractTerrain {

	public Sand(){
		super();
		// wheat
		this.color = new Color(245,222,179);
	}
	
	/* (non-Javadoc)
	 * @see terrain.Terrain#getColor()
	 */
	@Override
	public Color getColor() {
		return this.color;
	}
	
	/* (non-Javadoc)
	 * @see terrain.Terrain#toString()
	 */
	@Override
	public String toString() {
		return "G";
	}
	
}
