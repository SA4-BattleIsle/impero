package ch.usi.inf.sa4.impero.hexMap;

/**
 * 
 * @author Luca
 *
 */
public class Coordinate {
	private final int x;
	private final int y;
	
	public Coordinate(int x, int y){
		this.x = x;
		this.y = y;
	}
	
	public int getX(){
		return this.x;
	}
	
	public int getY(){
		return this.y;
	}

	@Override
	public String toString() {
		return "X:"+ this.x + " Y:" + this.y;
	}
	
	
	@Override
	public boolean equals(Object obj) {
		if (obj instanceof Coordinate) {
			return (this.x == ((Coordinate)obj).x && this.y == ((Coordinate)obj).y);
		}
		return false;
	}
	
	@Override
	public int hashCode() {
		return (this.x+this.y);
	}
}
