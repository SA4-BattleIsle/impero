package ch.usi.inf.sa4.impero.hexMap.terrain;

import java.awt.Color;

/**
 * @author Kasim and Jesper
 *
 */
public class DeepSea extends AbstractTerrain {
	public DeepSea(){
		super();
		// navy blue
		this.color = new Color(0,0,128);
	}
	
	/* (non-Javadoc)
	 * @see terrain.Terrain#getColor()
	 */
	@Override
	public Color getColor() {
		return this.color;
	}



	/* (non-Javadoc)
	 * @see terrain.Terrain#toString()
	 */
	@Override
	public String toString() {
		return "W";
	}

}
