package ch.usi.inf.sa4.impero.utility;

import java.awt.Point;

import ch.usi.inf.sa4.impero.hexMap.Coordinate;

/**
 * @author jesper
 *
 */
public class PixelHexMapConverter {
	
	/**
	 * @param clickPoint
	 * @param hexagonRadius
	 * @return
	 */
	public static Coordinate PixelCoordinateToHexMapCoordinate(Point clickPoint, int hexagonRadius) {
		
		// subtract the offset
		double hexagonHeight = hexagonHeight(hexagonRadius);
		double horizontalDistance = hexagonHorizontalDistance(hexagonRadius);
		int xPos = clickPoint.x - hexagonRadius;
		int yPos = clickPoint.y - (int) (hexagonHeight / 2);
		
		// get estimate hexagon coordinate		
		double q = xPos / horizontalDistance;
		double r = yPos / hexagonHeight;
				
		// convert even-q offset to cube
		double x = q;
		double z = r - q / 2.0;
		double y = - x - z;
		
		// find nearest cell
		HexMapCubeCoordinate cube = hexCubeCoordinateRound(x,y,z);
		
		// return even q offset coordinate
		
		Coordinate c = cube.getEvenQCoordinate();
		
		if(c.getX() % 2 != 0){
			c = new Coordinate(c.getX(), c.getY() -1 );
		}
		
		return c;	
	}
	
	/**
	 * @param c
	 * @return
	 */
	private static HexMapCubeCoordinate hexCubeCoordinateRound(double x, double y, double z) {
	    
		// rounding algorithm
		int rx = (int) Math.round(x);
	    int ry = (int) Math.round(y);
	    int rz = (int) Math.round(z);

	    double x_diff = Math.abs(rx - x);
	    double y_diff = Math.abs(ry - y);
	    double z_diff = Math.abs(rz - z);

	    if (x_diff > y_diff && x_diff > z_diff)
	        rx = - ry - rz;
	    else if (y_diff > z_diff)
	        ry = - rx - rz;
	    else
	        rz = - rx - ry;

	    return new HexMapCubeCoordinate(rx, ry, rz);
	}
	
	/**
	 * @param coordinate
	 * @param hexagonRadius
	 * @return
	 */
	public static Point HexMapCoordinateToPixelCoordinate(Coordinate coordinate, int hexagonRadius) {
		
		return HexMapCoordinateToPixelCoordinate(coordinate.getX(), coordinate.getY(), hexagonRadius);
	}
	
	/**
	 * @param x
	 * @param y
	 * @param hexagonRadius
	 * @return
	 */
	public static Point HexMapCoordinateToPixelCoordinate(int x, int y, int hexagonRadius) {
		
		double horizontalDistance = hexagonHorizontalDistance(hexagonRadius);
		double hexagonHeight = hexagonHeight(hexagonRadius);
		
		return HexMapCoordinateToPixelCoordinate(x, y, hexagonRadius, horizontalDistance, hexagonHeight);
	}
	
	/**
	 * @param x
	 * @param y
	 * @param hexagonRadius
	 * @param horizontalDistance
	 * @param hexagonHeight
	 * @return
	 */
	public static Point HexMapCoordinateToPixelCoordinate(
			int x, int y, int hexagonRadius, double horizontalDistance, double hexagonHeight) {
		
		int xPos = (int) (x * horizontalDistance) + hexagonRadius;
		int yPos = (int) (y * hexagonHeight) + (int) (hexagonHeight / 2);
		
		if(x % 2 != 0){
			yPos = (int) (yPos + hexagonHeight / 2);
		}
				
		return new Point(xPos, yPos);
	}
	
	
	/**
	 * @param radius
	 * @return
	 */
	public static double hexagonHeight(int radius) {
		return Math.sqrt(3.0)/2.0 * 2 * radius;
	}
	
	/**
	 * @param radius
	 * @return
	 */
	public static int hexagonWidth(int radius) {
		return radius * 2;
	}
	
	/**
	 * @param radius
	 * @return
	 */
	public static double hexagonHorizontalDistance(int radius) {
		return 0.75 * 2 * radius;
	}
	
	/**
	 * @param radius
	 * @return
	 */
	public static double hexagonVerticalDistance(int radius) {
		return hexagonHeight(radius);
	}

}
