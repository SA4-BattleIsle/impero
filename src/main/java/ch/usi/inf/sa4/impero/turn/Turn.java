package ch.usi.inf.sa4.impero.turn;

import java.util.LinkedList;
import java.util.List;

import ch.usi.inf.sa4.impero.action.Action;

/**
 * Class representing a turn performed by a player
 * 
 * @author Kasim
 * 
 */
public class Turn {
	private int actionPoints;
	private List<Action> actions = new LinkedList<Action>();

	public Turn(int actionPointsPerTurn) {
		this.actionPoints = actionPointsPerTurn;
	}

	/**
	 * Try to add an Action to the list of Actions
	 * 
	 * @param a
	 *            Actions to be add
	 * @return true if successfully add or false if action points were not
	 *         enough
	 */
	public boolean addAction(Action a) {
		 	if(this.actionPoints >= a.getActionPointCost())
		 	{ 
		 		this.actions.add(a);
		 		this.actionPoints -= a.getActionPointCost();
		 		return true; 
		 	}
		 	else
		 	{
		 		return false;
		 	}
	}

	/**
	 * Peek an Action out of the list of Actions
	 * 
	 * @return Action
	 */

	public Action pollAction() {
		return ((LinkedList<Action>) this.actions).pollFirst();
	}

	/**
	 * Return the size of the list of Actions contained into the turn
	 * 
	 * @return size of the list of Actions
	 */

	public int size() {
		return this.actions.size();
	}

	public int getActionPoints() {
		return actionPoints;
	}

	public void setActionPoints(int actionPointsPerTurn) {
		this.actionPoints = actionPointsPerTurn;
	}

	/**
	 * Return the list of actions contained in the turn.
	 * @return the list of actions
	 */
	public List<Action> getActions() {
		return this.actions;
	}

}
