package ch.usi.inf.sa4.impero.action;

import java.util.LinkedList;
import java.util.List;

import ch.usi.inf.sa4.impero.game.Game;

/**
 * Class for manage a sequence of Actions
 * @author Kasim
 *
 */

public class ActionSequence {
	private int sequenceNumber;
	
	private List<Action> actions = new LinkedList<Action>();

	public ActionSequence(int sequenceNumber){
		this.sequenceNumber = sequenceNumber;
	}
	
	/**
	 * Add action to ActionSequence
	 * @param a Action to the added 
	 */
	
	public void addAction(Action a){
		this.actions.add(a);
	}
	
	/**
	 * Returns the sequenceNumber of the ActionSequence
	 * @return and integer representing the sequenceNumber of the ActionSequence
	 */
	public int getSequenceNumber(){
		return this.sequenceNumber;
	}
	
	/**
	 * Peek action from the head of the queue
	 * @return Action peeked
	 */
	
	public Action peekAction(){
		return ((LinkedList<Action>)this.actions).pollFirst();
	}

	/**
	 * Execute all the actions of the ActionSequence in the context of the given game
	 * @param g Game where actions are executed
	 */
	public void execute(Game g){
		//TODO: And yet we have a problem... actions inside the same ActionSequence have to be
		//executed at the same time... (or...something else)
		while(this.actions.size()>0)
			this.peekAction().execute(g);
	}
	/**
	 * Returns the list of the actions contained in the action sequence
	 * @return
	 */
	public List<Action> getActions() {
		return this.actions;
	}

	public void removeAction(Action a) {
		this.actions.remove(a);
	}
	
}
