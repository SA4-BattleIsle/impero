package ch.usi.inf.sa4.impero.action.user;

import ch.usi.inf.sa4.impero.entity.AbstractBuilding;
import ch.usi.inf.sa4.impero.game.Game;
import ch.usi.inf.sa4.impero.game.Player;
import ch.usi.inf.sa4.impero.hexMap.Coordinate;

/**
 * This class is used to place the initial headquarter without any cost.
 * 
 * @author Luca
 *
 */
public class BuildStartingHeadQuartersAction extends BuildAction {
	
	/**
	 * Constructor.
	 * 
	 * @param where
	 * @param what2
	 */
	public BuildStartingHeadQuartersAction(Coordinate where, Class<? extends AbstractBuilding> what2, Player owner) {
		super(where, what2,owner);
	}
	
	@Override
	public int getActionPointCost() {
		return 0;
	}

	@Override
	public int getEnergyCost() {
		return 0;
	}
	
	@Override
	public void execute(Game game) {
		super.execute(game);
	}
}
