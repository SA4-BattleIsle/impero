package ch.usi.inf.sa4.impero.GUI;

import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

import javax.swing.JMenuItem;

import ch.usi.inf.sa4.impero.action.user.BuildAction;
import ch.usi.inf.sa4.impero.action.user.UnitCreateAction;
import ch.usi.inf.sa4.impero.entity.AbstractBuilding;
import ch.usi.inf.sa4.impero.game.Player;
import ch.usi.inf.sa4.impero.hexMap.Coordinate;

public class CreateBuildingJMenuItem extends JMenuItem {

	public CreateBuildingJMenuItem(final Class<? extends AbstractBuilding> c,
			final Coordinate cell, final Player activePlayer) {
		this.setText("Build "+ c.getSimpleName());
		
		this.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseReleased(MouseEvent e) {
				//Create new UnitCreateAction based on the class type specified
				BuildAction ba = new BuildAction(cell, c , activePlayer);
				activePlayer.addAction(ba);
			}
		});		
	}

}
