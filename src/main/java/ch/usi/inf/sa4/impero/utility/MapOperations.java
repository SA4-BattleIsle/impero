package ch.usi.inf.sa4.impero.utility;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.Stack;

import ch.usi.inf.sa4.impero.action.cost.MovementCostMapper;
import ch.usi.inf.sa4.impero.entity.AbstractUnit;
import ch.usi.inf.sa4.impero.hexMap.Coordinate;
import ch.usi.inf.sa4.impero.hexMap.HexMap;
import ch.usi.inf.sa4.impero.turn.Turn;

public class MapOperations {

	/**
	 * Return the list of the neighbor cells in some range
	 * 
	 * @param r - x coordinate
	 * @param q - y coordinate
	 * @param scale - range in cells
	 * @return list of points
	 */
	public static List<Coordinate> getNeighborsInRange(Coordinate c, int scale, int width, int height){
		int r = c.getX();
		int q = c.getY();
		List<Coordinate> ret = new ArrayList<Coordinate>();
		if(scale == 1){
			ret = MapOperations.getNeighbors(c, width, height);
		}else{
			//Converts coordinate
			int x = q;
			int z = r - (q + (q&1)) / 2;
			int y = -x-z;
			for (int i = x-scale; i < x+scale; i++) {
				for (int j = y-scale; j < y+scale; j++) {
					for (int j2 = z-scale; j2 < z+scale; j2++) {
						if(i+j+j2==0){
							//Convert back coordinates
								int qq = i;
								int rr = j2 + (i + (i&1)) / 2;
							//Add the element
							if(qq>=0 & qq < width)
								if(rr>=0 & rr < height)
									ret.add(new Coordinate(rr,qq));					
						}
					}
				}
			}	
		}
		return ret;
	}
	
	/**
	 * Return the list of the neighbor cells 
	 * 
	 * @param x - x coordinate
	 * @param y - y coordinate
	 * @param scale - range (deprecated, fixed to 1)
	 * @return list of points
	 */
	public static List<Coordinate> getNeighbors(Coordinate c, int width, int height){
		int x = c.getX();
		int y = c.getY();
		List<Coordinate> ret = new ArrayList<Coordinate>();
		
		if(x % 2 == 0 && y % 2 == 0)
		{
			// Look north
			if(y >= 1)
				ret.add(new Coordinate(x, y - 1));
			// Look north-east
			if(y >= 1 && x < width - 2){
				ret.add(new Coordinate(x + 1, y - 1));
			}
			// Look north-west
			if(x >= 1 && y >= 1)
				ret.add(new Coordinate(x - 1, y - 1));
			// Look south
			if(y < height - 2){
				ret.add(new Coordinate(x, y + 1));
				}
			// Look south-east
			if(x < width - 2){
				ret.add(new Coordinate(x + 1, y));
			}
			// Look south-west
			if(x >= 1){
				ret.add(new Coordinate(x - 1, y));
			}
		}else if(x % 2 == 0 && y % 2 != 0){
			// Look north
			if(y >= 1)
				ret.add(new Coordinate(x, y - 1));
			// Look north-east
			if(y >= 1 && x < width - 2)
				ret.add(new Coordinate(x + 1, y - 1));
			// Look north-west
			if(y >= 1 && x >= 1)
				ret.add(new Coordinate(x - 1, y - 1));
			// Look south
			if(y < height - 2)
				ret.add(new Coordinate(x, y + 1));
			// Look south-east
			if(x < width - 2)
				ret.add(new Coordinate(x + 1, y));
			// Look south-west
			if(x >= 1)
				ret.add(new Coordinate(x - 1, y));
		} else if(x % 2 != 0 && y % 2 != 0) {
			// Look north
			if(y >= 1)
				ret.add(new Coordinate(x, y - 1));
			// Look north-east
			if(x < width - 2)
				ret.add(new Coordinate(x + 1, y));
			// Look north-west
			if(x >= 1)
				ret.add(new Coordinate(x - 1, y));
			// Look south
			if(y < height - 2)
				ret.add(new Coordinate(x, y + 1));
			// Look south-east
			if(x < width - 2 && y < height - 2)
				ret.add(new Coordinate(x + 1, y + 1));
			// Look south-west
			if(y < height - 2 && x >= 1)
				ret.add(new Coordinate(x - 1, y + 1));
		} else {
			// Look north
			if(y >= 1)
				ret.add(new Coordinate(x, y - 1));
			// Look north-east
			if(x < width - 2){
				ret.add(new Coordinate(x + 1, y));
			}
			// Look north-west
			if(x >= 1)
				ret.add(new Coordinate(x - 1, y));
			// Look south
			if(y < height - 2){
				ret.add(new Coordinate(x, y + 1));
				}
			// Look south-east
			if(y < height - 2 && x < width - 2){
				ret.add(new Coordinate(x + 1, y + 1));
			}
			// Look south-west
			if(x >= 1 && y < height - 2){
				ret.add(new Coordinate(x - 1, y + 1));
			}
		}
		return ret;
	}
	
	public static Set<Coordinate> traverse(Coordinate currentCoordinate, Set<Coordinate> visitedCoordinates, Set<Coordinate> foundCoordinates, 
			int remainingActionPoints, HexMap map, Class<? extends AbstractUnit> unit) {
		foundCoordinates.add(currentCoordinate);
		List<Coordinate> neighbors = getNeighbors(currentCoordinate, map.getWidth(), map.getHeight());
		
		for (Coordinate coordinate : neighbors) {	
			int cost = MovementCostMapper.getActionPointCost(unit, map.getTerrainType(coordinate));
			if(cost >= 1 && remainingActionPoints-cost >= 0 && !visitedCoordinates.contains(coordinate)) {
					visitedCoordinates.add(currentCoordinate);
				traverse(coordinate, visitedCoordinates, foundCoordinates, remainingActionPoints-cost, map, unit);
			}
		}
		return foundCoordinates;
	}
	
	public static List<Coordinate> getShortestPath(Coordinate start, Coordinate end, HexMap map, Turn turn, Class<? extends AbstractUnit> unit) {
		List<Coordinate> possibleCoordinates = map.getPossibleCellDestinations(start, turn, unit);
		possibleCoordinates.add(start);
		Map<Coordinate, Integer> dist = new HashMap<Coordinate, Integer>();
		Map<Coordinate, Coordinate> precedent = new HashMap<Coordinate, Coordinate>();
		//initialization
		for (Coordinate coordinate : possibleCoordinates) {
			if(coordinate.equals(start)) {
				dist.put(coordinate, 0);
			} else {
				dist.put(coordinate, Integer.MAX_VALUE);
			}
		}
		
		precedent.put(start, start);
		List<Coordinate> q = new ArrayList<Coordinate>(possibleCoordinates);
		while(q.size() > 0) {
			Coordinate u = findSmallest(dist, q);
			q.remove(u);
			if(dist.get(u) == Integer.MAX_VALUE) {
				break;
			}
			if(u.equals(end)) {
				break;
			}
			List<Coordinate> neighbors = MapOperations.getNeighbors(u, map.getWidth(), map.getHeight());
			for (Coordinate coordinate : neighbors) {
				if(possibleCoordinates.contains(coordinate)) {
					int distBet = dist.get(u) + MovementCostMapper.getActionPointCost(unit, map.getTerrainType(coordinate));
					if(distBet < dist.get(coordinate) && distBet <= turn.getActionPoints()) {
						dist.put(coordinate, distBet);
						precedent.put(coordinate, u);
					}
				}
			}
		}
		Stack<Coordinate> stack = new Stack<Coordinate>();
		Coordinate w = end;
		while(precedent.get(w) != start) {
			stack.push(w);
			w = precedent.get(w);
		}
		stack.push(w);
		List<Coordinate> results = new ArrayList<Coordinate>(stack);
		Collections.reverse(results);
		return results;
		}
	
	private static Coordinate findSmallest(Map<Coordinate, Integer> dist, List<Coordinate> q) {
		int min = Integer.MAX_VALUE;
		Coordinate minIndex = null;
		Iterator<Entry<Coordinate, Integer>> it = dist.entrySet().iterator();
		
		while(it.hasNext()) {
			Map.Entry<Coordinate, Integer> pair = (Map.Entry<Coordinate, Integer>) it.next();
			if(q.contains(pair.getKey())) {
				if(pair.getValue() < min) {
					min = pair.getValue();
					minIndex = pair.getKey();
				}
			}
		}
		return minIndex;
	}	
}
	

