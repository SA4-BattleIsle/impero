package ch.usi.inf.sa4.impero.action.game;

import java.util.List;

import ch.usi.inf.sa4.impero.entity.EnergyExtractionFacility;
import ch.usi.inf.sa4.impero.entity.Entity;
import ch.usi.inf.sa4.impero.game.Game;

/**
 * @author 
 *
 */

public class BeginTurnAction extends AbstractGameAction {
	@Override
	public void execute(Game game) {
		//Assign the initial amount of action point per turn to the players
		for(int x = 0; x < game.getNumberOfPlayers() ; x++ ){
			game.getPlayerByIndex(x).setStartingActionPoints(game.getActionPointsPerTurn());
		}
		this.gatherEnergy(game);
	}

	@Override
	public int getActionPointCost() {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public int getEnergyCost() {
		// TODO Auto-generated method stub
		return 0;
	}
	
	/**
	 * Gathers energy from EnergyExtractorFacility and distribute it to the owner
	 */
	private void gatherEnergy(Game game){
		//Get the list of the EnergyExtractorFacility currently available on the map
		List<Entity> facilities = game.getMap().getEntitiesByClass(EnergyExtractionFacility.class);
		for(Entity f:facilities){
			int energy =((EnergyExtractionFacility)f).getBonus();
			f.getOwner().addEnergy(energy);
			game.logEvent(f.getOwner().toString() + " got " + energy + " energy from his EEF.");
		}
	}

	public String toString(){
		return "-- Begin of Turn --";
	}
}
