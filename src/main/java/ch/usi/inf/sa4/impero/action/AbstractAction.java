package ch.usi.inf.sa4.impero.action;

/**
 * @author Kasim
 * 
 */

public abstract class AbstractAction implements Action {

	private boolean enabled = true;

	@Override
	public boolean getEnabled() {
		return this.enabled;
	}

	@Override
	public void setEnabled(boolean status) {
		this.enabled = status;
	}

}
