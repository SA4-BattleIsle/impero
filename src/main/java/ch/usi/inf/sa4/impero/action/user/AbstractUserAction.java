package ch.usi.inf.sa4.impero.action.user;

import ch.usi.inf.sa4.impero.action.AbstractAction;
import ch.usi.inf.sa4.impero.game.Player;
import ch.usi.inf.sa4.impero.hexMap.Coordinate;

/**
 * @author Kasim
 * 
 */

public abstract class AbstractUserAction extends AbstractAction {
	protected boolean enabled = true;
	protected Player owner;
	protected Coordinate location;

	public AbstractUserAction(Player owner, Coordinate location) {
		this.owner = owner;
		this.location = location;
	}
	
	/**
	 * @return the location of the action
	 */
	public Coordinate getLocation() {
		return location;
	}
	
	

}
