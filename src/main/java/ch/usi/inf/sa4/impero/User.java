package ch.usi.inf.sa4.impero;

/**
 * @author 
 *
 */

public class User {
	
	private final String firstName;
	private final String lastName;
	
	public User(final String firstName, final String lastName) {
		this.firstName = firstName;
		this.lastName = lastName;
	}

}
