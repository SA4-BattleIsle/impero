package ch.usi.inf.sa4.impero.GUI;

import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

import javax.swing.JMenuItem;

import ch.usi.inf.sa4.impero.action.user.UnitCreateAction;
import ch.usi.inf.sa4.impero.entity.AbstractUnit;
import ch.usi.inf.sa4.impero.game.Player;
import ch.usi.inf.sa4.impero.hexMap.Coordinate;

public class CreateUnitJMenuItem extends JMenuItem {
	
	public CreateUnitJMenuItem(final Class<? extends AbstractUnit> c, final Coordinate fromwhat, final Player owner){
		this.setText("Create " + c.getSimpleName());
		this.addMouseListener(new MouseAdapter() {
			
			@Override
			public void mouseReleased(MouseEvent e) {
				//Create new UnitCreateAction based on the class type specified
				UnitCreateAction uca = new UnitCreateAction(fromwhat, c, owner);
				owner.addAction(uca);
			}
		});
	}

	
}
