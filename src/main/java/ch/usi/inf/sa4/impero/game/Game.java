package ch.usi.inf.sa4.impero.game;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Queue;

import ch.usi.inf.sa4.impero.action.Action;
import ch.usi.inf.sa4.impero.action.ActionSequence;
import ch.usi.inf.sa4.impero.action.game.BeginTurnAction;
import ch.usi.inf.sa4.impero.action.game.StartUpAction;
import ch.usi.inf.sa4.impero.entity.AbstractUnit;
import ch.usi.inf.sa4.impero.entity.Entity;
import ch.usi.inf.sa4.impero.hexMap.Coordinate;
import ch.usi.inf.sa4.impero.hexMap.HexMap;
import ch.usi.inf.sa4.impero.turn.TurnMerger;

/**
 * 
 * @author Luca, Jesper, Kasim , Talal
 *
 */

public class Game {
	private final List<GameObserver> observers = new ArrayList<GameObserver>();
	private final int actionPointsPerTurn;
	private final List<Player> players = new ArrayList<>();
	private Player activePlayer;

	private final Queue<ActionSequence> actionQueue = new LinkedList<ActionSequence>();
	private final ActionSequence systemActionSequence = new ActionSequence(0);
	private final HexMap map;
	
	public Game (final int actionPointsPerTurn, final HexMap map, final String firstName, final String lastName){
    	this.map = map;
    	this.actionPointsPerTurn = actionPointsPerTurn;
    	
    	// add the user who created the game
    	this.players.add(new Player(firstName, lastName, actionPointsPerTurn));
    	// set the creator as active player
    	this.activePlayer = this.players.get(0);
    }
    
	/**
	 * Add player to the actual game
	 * @param player
	 */
    public void addPlayer(Player player){
    	this.players.add(player);
    }
    
    /** 
     * Add observer to the list of observers
     * @param obs
     */
    
    public void addObserver(GameObserver obs){
    	this.observers.add(obs);
    }
    
    /**
     * Notify to the observers when the map has to be redrawn
     */
    public void refreshMapDisplayRequired(){
    	for(GameObserver obs: this.observers){
    		obs.refreshMapDisplayRequired();
    	}
    }
    
    /**
     * Notify to the observers when the entity list has to be refreshed
     */
    public void refreshEntityListDisplayRequired(){
    	for(GameObserver obs: this.observers){
    		obs.refreshEntityListDisplayRequired();
    	}
    }

    
    /**
     * Notify to the observers when there is a new log entry to be displayed
     */
    public void logEvent(String text){
    	for(GameObserver obs: this.observers){
    		obs.logEvent(text);
    	}
    }
    
    public void gameStarted() {
    	for(GameObserver obs: this.observers){
    		obs.gameStarted();
    	}
    }
    
    public void activePlayerChanged() {
    	for(GameObserver obs: this.observers){
    		obs.activePlayerChanged();
    	}
    }
    
    /**
     * Add action to the reserved systemActionSequence (SeqNum=0)
     * @param a
     */
    
    public void addAction(Action a){
    	this.systemActionSequence.addAction(a);
    }
    
    /**
     * Returns the number of player playing the game
     * @return number of players
     */
    public int getNumberOfPlayers() {
    	return players.size();
    }

    /**
     * Start the actual game by scheduling and executing the system actions
     */
    public void start(){
    	//Adds StartupAction at the beginning of the turn
    	Action sua = new StartUpAction();
    	//Adds first BeginTurnAction
    	Action bta = new BeginTurnAction();
    	this.addAction(sua);
    	this.addAction(bta);
    	this.actionQueue.add(this.systemActionSequence);
    	//Invoke execution of the first ActionSequence
    	this.execute();
    	this.gameStarted();
    }
    
    /**
     * Executes all the ActionSequence in the queue in the correct order
     */
    public void execute(){
    	//Merges Turn into ActionSequence
    	TurnMerger tm = new TurnMerger();
    	for(Player p : this.players){
    		tm.addTurn(p.getTurn());
    		p.resetTurn();
    	}
    	
    	this.actionQueue.addAll(tm.merge(this));
    	System.out.println("AQ:"+this.actionQueue.size());
    	
    	//Executes them in the correct order
    	while(this.actionQueue.size()>0){
    		ActionSequence tbe = this.actionQueue.poll();
    		tbe.execute(this);
    	}
    	
    	
    }
    
    
    public void beforeTurn(){
    	Action bta = new BeginTurnAction();
    	this.addAction(bta);
    	this.actionQueue.add(this.systemActionSequence);
    	//Invoke execution of the first ActionSequence
    	this.execute();
    }
    
    /**
     * Returns the map of the actual game
     * @return map
     */
    public HexMap getMap() {
    	return map;
    }
    
    /**
     * Add the given entity at the given coordinate.
     * 
     * @param where
     * @param what
     */
    public void addEntity(Coordinate where, Entity what) {
    	map.addEntity(where, what);
    }
    
    /**
     * Check if the given coordinate is occupied by an entity.
     * 
     * @param c
     * @return true if an entity is at this coordinate, false otherwise
     */
    public boolean coordinateHasEntity(Coordinate c) {
    	return map.coordinateHasEntity(c);
    }
    
    /**
     * Find a free cell around the given coordinate.
     * 
     * @param c
     * @return the first free cell around the given coordinate
     */
    public Coordinate findFreeCoordinate(Coordinate c, Class<? extends AbstractUnit> unit) {
    	return map.findFreeCoordinateAround(c, unit);
    }
    
    /**
     * get the first player in the list.
     * 
     * @return the first player in the list
     */
    public Player getPlayerByIndex(int index){
    	return players.get(index);
    }
    
	/**
	 * get the list of players
	 * 
	 * @return the players
	 */
	public List<Player> getPlayers() {
		return players;
	}
    
    /**
     * returns the amount of action point for turn to be given out to the players
     * @return amount of action points
     */
	public int getActionPointsPerTurn() {
		return this.actionPointsPerTurn;
	} 
	
	/**
	 * @return the activePlayer
	 */
	public Player getActivePlayer() {
		return activePlayer;
	}

	/**
	 * @param activePlayer the activePlayer to set
	 */
	public void setActivePlayer(Player activePlayer) {
		if (this.players.contains(activePlayer)) {
			this.activePlayer = activePlayer;
			this.activePlayerChanged();
			this.logEvent("active player " + activePlayer.getFirstName());
			this.refreshMapDisplayRequired();
			this.refreshEntityListDisplayRequired();
		}
	}
	
	/**
	 * @param hashCode the hashCode of the active player
	 */
	public void setActivePlayer(int hashCode) {
		for (Player p : this.players) {
			if (p.hashCode() == hashCode) {
				this.setActivePlayer(p);
				break;				
			}
		}
	}

	
	
}
