package ch.usi.inf.sa4.impero.GUI;

import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Point;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.util.List;
import java.util.Map;

import javax.swing.JPanel;
import javax.swing.SwingUtilities;

import ch.usi.inf.sa4.impero.action.user.BuildAction;
import ch.usi.inf.sa4.impero.action.user.MoveAction;
import ch.usi.inf.sa4.impero.entity.AbstractUnit;
import ch.usi.inf.sa4.impero.entity.Entity;
import ch.usi.inf.sa4.impero.game.Game;
import ch.usi.inf.sa4.impero.hexMap.Coordinate;
import ch.usi.inf.sa4.impero.hexMap.HexMap;
import ch.usi.inf.sa4.impero.hexMap.terrain.DeepSea;
import ch.usi.inf.sa4.impero.hexMap.terrain.EnergyResource;
import ch.usi.inf.sa4.impero.hexMap.terrain.Field;
import ch.usi.inf.sa4.impero.hexMap.terrain.Forest;
import ch.usi.inf.sa4.impero.hexMap.terrain.Mountain;
import ch.usi.inf.sa4.impero.hexMap.terrain.Sand;
import ch.usi.inf.sa4.impero.hexMap.terrain.Sea;
import ch.usi.inf.sa4.impero.hexMap.terrain.Terrain;
import ch.usi.inf.sa4.impero.hexMap.terrain.TerrainType;
import ch.usi.inf.sa4.impero.utility.CompressedArray;
import ch.usi.inf.sa4.impero.utility.CompressedMatrix;
import ch.usi.inf.sa4.impero.utility.PixelHexMapConverter;

/**
 * 
 * @author Jesper
 *
 */
public class MapCanvas extends JPanel implements MouseListener {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -3966367378743307869L;
	
	private final Game game;
	private final HexMap map;
	private final LogPanel logger;
	private final Map<Coordinate,Entity> entities;
	private int hexMapWidth;
	private int hexMapHeight;
	private List<Coordinate> possibleMoves;
	
	private int hexagonRadius = 20;
	private int hexagonWidth = this.hexagonRadius * 2;
	private double hexagonHeight = Math.sqrt(3.0)/2.0 * this.hexagonWidth;
	private int canvasWidth;
	private int canvasHeight;
	
	private boolean clicked = false;
	private boolean doubleClicked = false;
	private Coordinate clickedCell;
	private JPanel parent;

	private AbstractUnit selectedUnit;
	
	/**
	 * MapCanvas visualizes a HexMap with the assigned terrains
	 * 
	 * @param map	the HexMap to be drawn
	 */
	public MapCanvas(final Game game, final LogPanel logger, JPanel parent){
		this.game = game;
		this.parent = parent;
		this.map = game.getMap();
		this.logger = logger;
		this.entities = map.getAllEntities();
		this.setBackground(Color.WHITE);
		this.changeCanvasSize();
		this.addMouseListener(this);
	}
	
	private void changeCanvasSize() {
		this.hexMapWidth = map.getWidth();
		this.hexMapHeight = map.getHeight();
		this.canvasWidth = (int)(1.5 * this.hexagonWidth * (this.hexMapWidth/2.0)) + (this.hexagonRadius / 2);
		this.canvasHeight = (int) ((this.hexagonHeight * this.hexMapHeight) + this.hexagonHeight / 2.0);
		this.setPreferredSize(new Dimension(canvasWidth, canvasHeight));
		this.setMaximumSize(new Dimension(canvasWidth, canvasHeight));
		this.setSize(canvasWidth, canvasHeight);
	}

	/* (non-Javadoc)
	 * @see javax.swing.JComponent#paintComponent(java.awt.Graphics)
	 */
	protected void paintComponent(Graphics g) {
        super.paintComponent(g);

        CompressedMatrix mapStructure = map.getMap();
		
		Graphics2D g2 = (Graphics2D) g;
		
		double horizontalDistance =  PixelHexMapConverter.hexagonHorizontalDistance(this.hexagonRadius);
		
		Terrain cell;
		Point p;
		//For each column
		for (int y = 0; y < this.hexMapHeight; y++) {
			
			final CompressedArray row = mapStructure.get(y);
			//For each row
			for (int x = 0; x < row.size(); x++) {

				TerrainType type = TerrainType.values()[row.get(x)];
				
				switch (type) {
				case DEEPSEA:
					cell = new DeepSea();
					break;
				case SEA:
					cell = new Sea();
					break;
				case SAND:
					cell = new Sand();
					break;
				case FOREST:
					cell = new Forest();
					break;
				case MOUNTAIN:
					cell = new Mountain();
					break;
				case FIELD:
					cell = new Field();
					break;
				case ENERGYRESOURCE:
					cell = new EnergyResource();
					break;
				default:
					cell = new Sea();
					break;
				}
			
				cell.setRadius(this.hexagonRadius);
				
				// get the pixel coordinate
				p =  PixelHexMapConverter.HexMapCoordinateToPixelCoordinate(
						x, y, this.hexagonRadius, horizontalDistance, this.hexagonHeight);
				
				// create a coordinate
				Coordinate c = new Coordinate(x, y);
				
				// draw the terrain
				cell.setCenter(p.x, p.y);
				
				Boolean activePlayerEntity = this.possibleMoves != null && this.possibleMoves.contains(c);
				
				Color color = (activePlayerEntity) ? Color.white : Color.black; 				
				g2.setPaint(color);
				g2.setStroke(new BasicStroke(2));
				g2.draw(cell.getPolygon());
				color = (activePlayerEntity) ? cell.getColor().brighter() : cell.getColor();
				g2.setPaint(color);				
				g2.fill(cell.getPolygon());
				
				// if there is an entity on the cell - draw the entity
				
				if (map.coordinateHasEntity(c)) {
					
					Entity entity = entities.get(c);
					
					entity.setWidth(this.hexagonRadius);
					// draw the entity
					entity.setCenter(p.x, p.y);
					g2.setPaint(Color.black);
					g2.setStroke(new BasicStroke(2));
					// indicate that active player is owner
					if( entity.getOwner().hashCode() == game.getActivePlayer().hashCode()){
						g2.setPaint(new Color(255,140,0));
						g2.setStroke(new BasicStroke(4));
					}
					g2.draw(entity.getShape());
					g2.setPaint(entity.getColor());
					g2.fill(entity.getShape());

				}
				
				// print cell coordinate for debug
				g2.setPaint(color.white);
				Font font = new Font("TimesRoman", Font.PLAIN, 10);
				g2.setFont(font);
				g2.setFont(font); 
				//g2.drawString(x + ", " + y, p.x - (this.hexagonRadius/2), p.y);
				
			}
		}
		
		// mark the clicked cell
		if (clicked) {
			// get the pixel coordinate
			p =  PixelHexMapConverter.HexMapCoordinateToPixelCoordinate(
					clickedCell.getX(), clickedCell.getY(), this.hexagonRadius, horizontalDistance, this.hexagonHeight);
			
			// draw the terrain
			cell = new Sea();
			cell.setRadius(this.hexagonRadius);
			cell.setCenter(p.x, p.y);
			g2.setPaint(Color.WHITE);
			g2.setStroke(new BasicStroke(4));
			g2.draw(cell.getPolygon());
			
		}
		
    }
	
	/**
	 * @return the hexagonRadius
	 */
	public int getHexagonRadius() {
		return hexagonRadius;
	}

	/**
	 * @param hexagonRadius the hexagon radius to set
	 */
	public void setHexagonRadius(int hexagonRadius) {
		this.hexagonRadius = hexagonRadius;
		this.hexagonWidth = PixelHexMapConverter.hexagonWidth(hexagonRadius);
		this.hexagonHeight = PixelHexMapConverter.hexagonHeight(hexagonRadius);
		this.changeCanvasSize();
	}

	@Override
	public void mouseClicked(MouseEvent e) {
		Coordinate c = PixelHexMapConverter.PixelCoordinateToHexMapCoordinate(e.getPoint(), this.hexagonRadius);
		this.clicked = true;
		this.clickedCell = c;
		
		if (SwingUtilities.isRightMouseButton(e)) {
		     e.consume();
		     //handle right click event.
		     this.doPop(e);
		}
		// user double clicked to move
		else if (this.doubleClicked && this.possibleMoves != null && this.possibleMoves.contains(c)) {
			this.doubleClicked = false;
			this.possibleMoves = null;
			game.logEvent("you created a move action");
			Coordinate from = map.getCoordinatesByEntity(this.selectedUnit);
			MoveAction move = new MoveAction(from, c, selectedUnit.getClass(), game.getActivePlayer());
			game.getActivePlayer().addAction(move);
		}
		// double click
		else if(e.getClickCount() == 2){
			Entity entity = map.getEntityByCoordinates(c);
			if (entity instanceof AbstractUnit && entity.getOwner().hashCode() == game.getActivePlayer().hashCode()) {
				this.doubleClicked = true;
				selectedUnit = (AbstractUnit) entity;
				this.possibleMoves = this.map.getPossibleCellDestinations(c, game.getActivePlayer().getTurn(), selectedUnit.getClass());
			}
		}
		
		// no double click
		if (e.getClickCount() != 2){
			this.doubleClicked = false;
			this.possibleMoves = null;
		}
				
		repaint();
		
	}
	
	public void setClickedCell(Coordinate c) {
		this.clicked = true;
		this.clickedCell = c;
		this.repaint();
	}

	@Override
	public void mousePressed(MouseEvent e) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void mouseReleased(MouseEvent e) {
		// TODO Auto-generated method stub
	}


	@Override
	public void mouseEntered(MouseEvent e) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void mouseExited(MouseEvent e) {
		// TODO Auto-generated method stub
		
	}

	/** 
	 * Displays a contextual menu near the cell
	 * @param e MouseEvent 
	 */
	private void doPop(MouseEvent e) {
		PopUpMenu menu = null;
		Coordinate cell = PixelHexMapConverter.PixelCoordinateToHexMapCoordinate(e.getPoint(), this.hexagonRadius);
		if(map.coordinateHasEntity(cell)){
			//Display entity options
			Entity entity = map.getEntityByCoordinates(cell);
			// only display if owner
			if (entity.getOwner().hashCode() == game.getActivePlayer().hashCode())
				menu = new PopUpMenu(entity, ((GamePanel)parent).getGame() , cell);

		}else{
			//Display terrain options
			TerrainType terrain = map.getTerrainType(cell);
			menu = new PopUpMenu(terrain,((GamePanel)parent).getGame() , cell);
		}
		if (menu != null)
			menu.show(e.getComponent(), e.getX() + 10, e.getY());
	}

}
