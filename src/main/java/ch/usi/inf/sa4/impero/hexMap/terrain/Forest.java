package ch.usi.inf.sa4.impero.hexMap.terrain;

import java.awt.Color;

/**
 * @author Kasim and Jesper
 *
 */
public class Forest extends AbstractTerrain {

	public Forest(){
		super();
		// forest green
		this.color = new Color(34,139,34);
	}
	
	/* (non-Javadoc)
	 * @see terrain.Terrain#toString()
	 */
	@Override
	public String toString() {
		return "F";
	}

	@Override
	public Color getColor() {
		return this.color;
	}
	
}
