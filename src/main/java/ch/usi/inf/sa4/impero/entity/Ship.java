package ch.usi.inf.sa4.impero.entity;

import java.awt.Color;

import ch.usi.inf.sa4.impero.game.Player;

/**
 * @author Talal
 *
 */

public class Ship extends AbstractNavalUnit {
	public Ship(final Player owner) {
		super(owner);
		super.health = 30;
		super.damage = 20;
		super.name = "Ship";
		// rosy brown
		this.color = new Color(112,128,144);
	}
}

