package ch.usi.inf.sa4.impero.game;

public interface PlayerObserver {

	public void refreshActionListDisplayRequired();

}
