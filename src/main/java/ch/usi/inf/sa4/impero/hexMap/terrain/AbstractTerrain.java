package ch.usi.inf.sa4.impero.hexMap.terrain;

import java.awt.Color;
import java.awt.Polygon;

/**
 * @author Kasim and Jesper
 *
 */
public abstract class AbstractTerrain implements Terrain {
	
	protected int x;
	protected int y;
	protected double height;
	protected Color color;
	protected Polygon poly = new Polygon();
	public int radius = 3;
	
	/**
	 * 
	 */
	public AbstractTerrain() {
		this.setCenter(0, 0);
	}
	
	/**
	 * A new terrain at position x,y
	 * 
	 * @param x - x coordinate
	 * @param y - y coordinate
	 */
	public AbstractTerrain(final int x, final int y) {
		this.setCenter(x, y);
	}

	/**
	 * set the radius of the polygon
	 * 
	 * @param radius
	 */
	public void setRadius(int radius){
		this.radius=radius;
	}
	
	
	/**
	 * get the radius of the hexagon cell
	 * 
	 * @return - radius of the hexagon cell
	 */
	public int getRadius(){
		return this.radius;
	}
	
	/**
	 * get the color of the terrain
	 * 
	 * @return
	 */
	abstract public Color getColor();
	
	/**
	 * 
	 * get the hexagon of the terrain/cell
	 * 
	 * @return
	 */
	public Polygon getPolygon() {
		return this.poly;
	}
	
	/**
	 * get the x position
	 * 
	 * @return
	 */
	public int getX(){
		return this.x;
	}
	
	/**
	 * get the y position
	 * 
	 * @return
	 */
	public int getY(){
		return this.y;
	}
	
	/**
	 * get the height
	 * 
	 * @return
	 */
	public double getHeight(){
		return this.height;
	}
	
	/**
	 * set the height
	 * 
	 * @param height
	 */
	public void setHeight(double height){
		this.height =height;
	}
	
	/**
	 * set the center of the terrain/cell (xx,yy)
	 * 
	 * @param xx
	 * @param yy
	 */
	public void setCenter(final int xx, final int yy) {	
		this.x = xx;
		this.y = yy;	
		this.generatePolygon();
	}
	
	/**
	 * generate a polygon representation of the hexagon cell
	 */
	private void generatePolygon() {
		
		int pointX;
		int pointY;
		
		this.poly.reset();
		
		for (int i = 1; i <= 6; i++) {
			pointX = (int) (this.radius * Math.cos(2 * Math.PI * i/6) + this.x);
			pointY = (int) (this.radius * Math.sin(2 * Math.PI * i/6) + this.y);
			this.poly.addPoint(pointX, pointY);
		}
		
	}
	
	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	abstract public String toString();
	
}
