package ch.usi.inf.sa4.impero.mapGeneration;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import ch.usi.inf.sa4.impero.hexMap.HexMap;
import ch.usi.inf.sa4.impero.hexMap.terrain.TerrainType;
import ch.usi.inf.sa4.impero.utility.CompressedArray;
import ch.usi.inf.sa4.impero.utility.CompressedMatrix;

/**
 * @author Kasim, Jesper
 *
 */
public class MapGenerator {

	private CompressedMatrix noise;
	
	private int width;
	private int height;
	
	private int deepSeaLevel;
	private int seaLevel;
	private int fieldLevel;
	private int forestLevel;
	private int mountainLevel;
	private int minLevel;
	private int maxLevel;
	
	private int deepSeaPercent;
	private int landPercent;
	private int waterPercent;
	private int mountainPercent;
	private int energyPercent;

	/**
	 * Generates a map with terrains.
	 * 
	 * 
	 * @param width				width (cells) of the map
	 * @param height			height (cells) of the map
	 * @param landPercent		percent of land
	 * @param waterPercent		percent of water on land
	 * @param mountainPercent	percent of mountain on land
	 * @param energyPercent		percent of energy
	 * @param lakes				generate lakes
	 * @param rivers			generate rivers
	 * @return					a generated hexMap
	 */
	public HexMap generateMap(final int width, final int height, 
			final int landPercent, final int waterPercent, final int mountainPercent, final int energyPercent, 
			final boolean lakes, final boolean rivers) {
		
		
		this.width = width;
		this.height = height;
		
		// initialize levels
		
		this.deepSeaLevel = 0;
		this.seaLevel = 0;
		this.fieldLevel = 0;
		this.forestLevel = 0;
		this.mountainLevel = 0;		
		this.minLevel = 0; 
		this.maxLevel = 0;
		
		this.deepSeaPercent = 60;
		this.landPercent = landPercent;
		this.waterPercent = waterPercent;
		this.mountainPercent = mountainPercent;
		this.energyPercent = energyPercent;
		
		this.noise = new CompressedMatrix(width, height);
		
		final CompressedMatrix map = new CompressedMatrix();
		
		generateNoise(width, height);
		
		this.calcLevels();

		if(rivers)
			this.generateRiver(height, width);
		
		
		// iterate over rows
		for (int y = 0; y < height; y++) {
			
			CompressedArray row = new CompressedArray();
			int prevType = -1;
			int count = 0;

			for (int x = 0; x < width; x++) {
				
				// make sure the borders of the map are water
				
				int noiseValue = noise.getByCoordinate(x, y);
				
				if ( noiseValue > this.seaLevel && ( y < 2 || y > (height - 3) || x < 2 || x > (width - 2))) {
					// ned to store also in noise so that we can correctly place other cells like ER
					noiseValue = this.seaLevel;
					noise.setByCoordinate(x, y, noiseValue);
				}
								
				TerrainType type;
				
				double terrainHeight = noiseValue;
				
				// assign DEEP SEA
				type = TerrainType.DEEPSEA;
				
				// assign SEA
				if(terrainHeight > deepSeaLevel && terrainHeight <= seaLevel){
					type = TerrainType.SEA;
				}
				// assign SAND
				else if(terrainHeight > seaLevel && terrainHeight < fieldLevel){
					type = TerrainType.SAND;
				}
				// assign FIELD
				else if(terrainHeight >= fieldLevel && terrainHeight < forestLevel){
					type = TerrainType.FIELD;
				}
				// assign FOREST
				else if(terrainHeight >= forestLevel && terrainHeight < mountainLevel){
					type = TerrainType.FOREST;
				}
				// assign MOUNTAIN
				else if(terrainHeight >= mountainLevel){
					type = TerrainType.MOUNTAIN;
				}
				
//				if(prevType == -1)
//					prevType = type;
//					
//				if(prevType != type){
//					//Add new slice
//					column.add(new ColumnSlice(prevType, count));
//					
//					//Reset count
//					count = 1;
//				}else{
//					// increment count
//					count++;
//				}
//				
//				// add last slice
//				if(i == this.noise.length-1){
//					column.add(new ColumnSlice(type, count));
//				}
//				
//				prevType = type;
				
				row.add(type.ordinal());
			}
			
			// add column to map
			map.add(row);
				
			}
		
		// place the Energy Resource cells
		this.placeEnergyResources(map);
		return new HexMap(map);
		
		}
	
	/**
	 * Generate random noise for the heights of the map
	 */
	private void generateNoise(final int width, final int height){
		generateOctavedSimplexNoise(8, 0.4f, 0.02f, width, height);
		generateGradient(width, height);
	}

	/**
	 * Generates random noise using the simplex noise algorithm with multiple octaves
	 * (multiple runs for each coordinate in the matrix)
	 * 
	 * @param octaves
	 * @param roughness - the roughness for each layer of noise (0-1)
	 * @param scale - the scale/frequency of the layers
	 */
	private void generateOctavedSimplexNoise(int octaves, float roughness, float scale, final int width, final int height){

	       float layerFrequency = scale;
	       float layerWeight = 1;

	       for (int octave = 0; octave < octaves; octave++) {
	    	   SimplexNoiseOctave sn = new SimplexNoiseOctave(octave);

	          //Calculate single layer/octave of simplex noise, then add it to total noise
	    	  int noiseValue;
	          for(int y = 0; y < height; y++){
	             for(int x = 0; x < width; x++){
	            	 if ( octave > 0) {
	            		 noiseValue = noise.getByCoordinate(x, y);
					}
	            	 else {
	            		 noiseValue = 0;
	            	 }
	            	 noiseValue += (int) 100 * (1 + (float) sn.noise(x * layerFrequency, y * layerFrequency) * layerWeight);
	            	 noise.setByCoordinate(x, y, noiseValue);
	             }
	          }
	          
	          //Increase variables with each incrementing octave
	           layerFrequency *= 2;
	           layerWeight *= roughness;
	           
	       }

	   }

	/**
	 * Adds a gradient matrix to the noise to generate an island like map
	 */
	private void generateGradient(final int width, final int height) {
				
		int midX = width/2;
		int midY = height/2;
		
		int distX;
		int distY;
		int gradient;
		int value;
		
		for (int y = 0; y < height; y++) {
			for (int x = 0; x < width; x++) {
				distX = midX - x;
				distY = midY - y;
				gradient = (int) (100 * (Math.sqrt(distX * distX + distY * distY) * 2 / Math.sqrt(midX * midY) - 0.7));
				value = ( 1 + noise.getByCoordinate(x, y)) / 2 - gradient;
				noise.setByCoordinate(x, y, value);
			}
		}
		
	}


	/**
	 * Calculate the terrain/height levels of the map
	 * 
	 */
	private void calcLevels() {
		//Store all heights into an array
		int seaPercent = 100 - this.landPercent;
		List<Integer> altitudes = new ArrayList<>();
		for(int y = 0; y < this.height; y++)
		{
			for(int x = 0; x < this.width; x++){
				altitudes.add(noise.getByCoordinate(x, y));
			}
		}
		//Sort the array
		Collections.sort(altitudes);
		
		// calculate the levels - sand is implicit between sea and field
		this.minLevel = altitudes.get(0);
		this.maxLevel = altitudes.get(altitudes.size()-1);
		
		// set the level of the sea
		int cellsLeft = altitudes.size();
		int seaIndex = cellsLeft * seaPercent / 100;
		this.seaLevel = altitudes.get(seaIndex);
		
		// set the level of the deep sea
		int deepSeaIndex = (int) (seaIndex * deepSeaPercent / 100);
		this.deepSeaLevel = altitudes.get(deepSeaIndex);
		
		// set the level of mountains
		cellsLeft -= seaIndex;
		int mountainIndex = altitudes.size() - cellsLeft * this.mountainPercent / 100;
		this.mountainLevel = altitudes.get(mountainIndex);
		
		// set the level of forest
		cellsLeft = mountainIndex - seaIndex;
		int forestIndex = mountainIndex - (int) (cellsLeft * Math.random() * 0.5);
		this.forestLevel = altitudes.get(forestIndex);
		
		// set the level of field
		cellsLeft = forestIndex - seaIndex;
		int fieldIndex = forestIndex - (int) (cellsLeft * Math.random() * 0.8);
		this.fieldLevel = altitudes.get(fieldIndex);
		 
	}
	
	private void placeEnergyResources(final CompressedMatrix map) {
		
		int numERCells = this.height * this.width * this.landPercent / 100 * this.energyPercent / 100;
		int xPos;
		int yPos;
		for (int i = 0; i < numERCells; i++) {
			xPos = (int) (this.width * Math.random());
			yPos = (int) (this.height * Math.random());
			// if the cell is not water make it an ER
			if (noise.getByCoordinate(xPos, yPos) > this.seaLevel) {
				map.setByCoordinate(xPos, yPos, TerrainType.ENERGYRESOURCE.ordinal());
			}
			// if it was water try to find another location
			else {
				i--;
			}
			
		}
		
	}


	/**
		 * Generates rivers from the highest point of the map to the sea (if possible)
		 */
		public void generateRiver(int height, int width){
//			// Find a path between the highest point and the water level
//			// Find the highest point coordinates
//			boolean found = false;
//			int maxX = 0;
//			int maxY = 0;
//			for (int y = 0; y < this.height && !found; y++) {
//				for (int x = 0; x < this.width && !found; x++) {
//					if(noise.getByCoordinate(x, y) == this.maxLevel){
//						maxY = y;
//						maxX = x;
//						found = true;
//					}
//				}
//			}
//			
//	//		System.out.println("Sea started "+maxX+","+maxY+"@"+ this.maxLevel);		
//			
//			int curX = maxX;
//			int curY = maxY;
//			
//			int lminX;
//			int lminY;
//			
//			int llminX;
//			int llminY;
//			
//			llminX = maxX;
//			llminY = maxY;
//			double llmin = Double.MAX_VALUE;
//			
//			int scale=1;
//			
//			//Start from there and while over water level choose the lowest neighbor and make it water.
//			while(noise.getByCoordinate(curX, curY) > this.seaLevel){
//				//Select the lowest neighbor
//				lminX = curX;
//				lminY = curY;
//				scale = 1;
//				boolean dontstop = true;
//				//Increase range of search if not found
//				while(((lminX == curX) & (lminY == curY)) & !(scale>10) & dontstop){
//					llmin = Double.MAX_VALUE;
//					Coordinate coordinate = new Coordinate(curX, curY);
//					List<Coordinate> n = MapOperations.getNeighborsInRange(coordinate, scale, width, height);
//					for(Coordinate c: n){
////						System.out.println("\t\tN:"+ (int)c.getX() +","+ (int)c.getY() + "@" + (this.noise[(int)c.getY()][(int)c.getX()]==this.minLevel?"Already river/lake":this.noise[(int)c.getY()][(int)c.getX()]));
//						//Find the minimum compared to current position
//						if((this.noise[(int)c.getY()][(int)c.getX()]<this.noise[lminY][lminX]) && (this.noise[(int)c.getY()][(int)c.getX()]>this.minLevel)){
//							lminX = (int)c.getX();
//							lminY = (int)c.getY();
//						}
//						//Find the minimum compared to max
//						if((this.noise[(int)c.getY()][(int)c.getX()]<llmin) && (this.noise[(int)c.getY()][(int)c.getX()]>this.minLevel)){
//							llminX = (int)c.getX();
//							llminY = (int)c.getY();
//							llmin = this.noise[(int)c.getY()][(int)c.getX()];
//						}
//					}
//					System.out.println("\tcur:"+curX+","+curY+"\tlmin:"+lminX+","+ lminY + "\tllmin:"+llminX+","+ llminY);
//					//If no lower point is found the water flood into lake 
//					if((lminX == curX) & (lminY == curY)){	
//						// Flood local min and move the center
//						if(llmin<Double.MAX_VALUE)
//						{
//							//Flood
//							System.out.println("Sea flooded smothly"+ (int)llminX +","+ (int)llminY + "@" + this.noise[llminY][llminX]);
//							//Move the center
//							lminY = llminY;
//							lminX = llminX;
//							if(lminY==curY && lminX==curX)
//								dontstop =false;
//						}else{
//							//Flood the entire range
//							for(int p=0; p<n.size();p++){
//								//Flood
//								if( this.noise[(int)n.get(p).getY()][(int)n.get(p).getX()] > this.minLevel){
//									System.out.println("Sea flooded "+ (int)n.get(p).getX() +","+ (int)n.get(p).getY() + "@" + this.noise[(int)n.get(p).getY()][(int)n.get(p).getX()]);
//									this.noise[(int)n.get(p).getY()][(int)n.get(p).getX()] = this.minLevel;						
//								}
//							}
//							//Try to find a good point outside the new lake by increasing the range
//							scale++;
//							System.out.println("New scale:"+scale);
//						}
//										
//					}else{
//						scale=1;
//					}
//				}
//				scale=1;
//				//Make the old point be water before swapping
//				System.out.println("Sea flows to "+ lminX +","+ lminY + "@" + this.noise[lminY][lminX]);
//				this.noise[curY][curX] = this.minLevel;
//				curX = lminX;
//				curY = lminY;			
//			}		
//			System.out.println("Sea arrived to "+ curX +","+ curY + "@"+ this.seaLevel +"("+this.noise[curY][curX]+")");
		}
	
}