package ch.usi.inf.sa4.impero.action.user;

import ch.usi.inf.sa4.impero.entity.AbstractUnit;
import ch.usi.inf.sa4.impero.entity.Entity;
import ch.usi.inf.sa4.impero.game.Game;
import ch.usi.inf.sa4.impero.game.Player;
import ch.usi.inf.sa4.impero.hexMap.Coordinate;

/**
 * @author Talal
 *
 */

public class AttackAction extends AbstractUserAction {
	private final Coordinate attackerCoordinates;
	private final Coordinate receiverCoordinates;
	
	public AttackAction(Coordinate attackerC, Coordinate receiverC ,Player owner) {
		super(owner, attackerC);
		this.attackerCoordinates = attackerC;
		this.receiverCoordinates = receiverC;
	}
	

	@Override
	/**
	 * Execute an attack the attacker sends the damage and the receiver will increase it own health.   
	 * @param Game 
	 * 
	 */
	public void execute(Game game) {
		Entity attacker = (AbstractUnit) game.getMap().getEntityByCoordinates(attackerCoordinates);
		Entity receiver = game.getMap().getEntityByCoordinates(receiverCoordinates);
		((AbstractUnit)attacker).attack(receiver);
	}

	@Override
	public int getActionPointCost() {
		return 0;
	}

	@Override
	public int getEnergyCost() {
		// TODO Auto-generated method stub
		return 0;
	}

	/**
	 * Returns attacker coordinates
	 * @return
	 */
	public Coordinate getAttacker() {
		return this.attackerCoordinates;
	}

	/**
	 * Returns attackee coordinates
	 * @return
	 */
	public Coordinate getAttackee() {
		// TODO Auto-generated method stub
		return null;
	}

	public String toString(){
		return attackerCoordinates.toString() + " attacks " + receiverCoordinates; 
	}
}
