package ch.usi.inf.sa4.impero.hexMap.terrain;

import java.awt.Color;

/**
 * 
 * @author Luca
 *
 */
public class Field extends AbstractTerrain {

	public Field(){
		super();
		// yellow green
		this.color = new Color(154,205,50);
	}
	
	@Override
	public Color getColor() {
		// TODO Auto-generated method stub
		return this.color;
	}

	@Override
	public String toString() {
		// TODO Auto-generated method stub
		return "F";
	}

}
