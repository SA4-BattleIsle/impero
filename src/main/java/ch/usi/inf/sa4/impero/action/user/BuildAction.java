
package ch.usi.inf.sa4.impero.action.user;


import ch.usi.inf.sa4.impero.action.cost.BuildingCostMapper;
import ch.usi.inf.sa4.impero.entity.AbstractBuilding;
import ch.usi.inf.sa4.impero.entity.Entity;
import ch.usi.inf.sa4.impero.game.Game;
import ch.usi.inf.sa4.impero.game.Player;
import ch.usi.inf.sa4.impero.hexMap.Coordinate;

/**
 * This class represent a build action. This action will place the given building at the given position. For instance this action does not
 * check if the building can be placed on the given coordinate.
 * 
 * @author Luca
 *
 */

public class BuildAction extends AbstractUserAction {
	//coordinate of the entity that will spawn the unit
	private final Coordinate where;
	//class of the unit that the player want create
	private final Class<? extends AbstractBuilding> what;
	
	/**
	 * Constructor. It take the coordinate where the player want to place the building, which building he want to create and the player that fired
	 * this action.
	 * 
	 * @param where
	 * @param what
	 */
	public BuildAction(Coordinate where, Class<? extends AbstractBuilding> what, Player owner) {
		super(owner, where);
		this.where=where;
		this.what=what;
	}
	

	
	@Override
	public void execute(Game game) {
		game.logEvent("Execute: "+ this.toString());
		Entity building = null;
		
		try {
			building = (AbstractBuilding) what.getConstructor(this.owner.getClass()).newInstance(this.owner);
		} catch (Exception e) {
			e.printStackTrace();
		} 
		game.addEntity(where, building);
		//Refresh of the entity list is required
		game.refreshEntityListDisplayRequired();
		//Refresh of the map is required
		game.refreshMapDisplayRequired();
	}
	
	@Override
	public int getActionPointCost() {
		return BuildingCostMapper.getActionPointCost(this.what);
	}

	public int getEnergyCost() {
		return BuildingCostMapper.getEnergyCost(this.what);
	}


	/**
	 * Return the coordinate coresponding to the place of built
	 * @return
	 */
	public Coordinate getDestination() {
		return this.where;
	}
	
	public String toString(){
		return "Build " + what.getSimpleName() + "@" + this.where;
	}

}
