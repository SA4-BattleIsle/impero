package ch.usi.inf.sa4.impero.GUI;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.swing.JMenuItem;

import ch.usi.inf.sa4.impero.entity.EnergyExtractionFacility;
import ch.usi.inf.sa4.impero.entity.HeadQuarter;
import ch.usi.inf.sa4.impero.entity.Port;
import ch.usi.inf.sa4.impero.game.Game;
import ch.usi.inf.sa4.impero.hexMap.Coordinate;
import ch.usi.inf.sa4.impero.hexMap.terrain.TerrainType;

/**
 * 
 * @author Kasim
 *
 */

public class MenuItemMapperBuilding {

	private  Map<TerrainType,List<JMenuItem> > map = new HashMap<TerrainType, List<JMenuItem>>() ;
	
	public MenuItemMapperBuilding(Game game, Coordinate cell) {
		//Ground buildings
		List<JMenuItem> groundoptions = new ArrayList<JMenuItem>();
		groundoptions.add(new CreateBuildingJMenuItem(HeadQuarter.class,cell,game.getActivePlayer()));
			
		map.put(TerrainType.SAND, groundoptions);
		map.put(TerrainType.FIELD, groundoptions);
		map.put(TerrainType.FOREST, groundoptions);
		map.put(TerrainType.MOUNTAIN, groundoptions);
		
		//Deep Sea buildings
		List<JMenuItem> wateroptions = new ArrayList<JMenuItem>();
		wateroptions.add(new JMenuItem("Nothing to be done here yet"));
		map.put(TerrainType.DEEPSEA, wateroptions );
		
		//Sea buildings
		List<JMenuItem> seaoptions = new ArrayList<JMenuItem>();
		seaoptions.add(new CreateBuildingJMenuItem(Port.class,cell,game.getActivePlayer()));
		map.put(TerrainType.SEA, seaoptions);
		
		//Special buildings
		List<JMenuItem> specialoptions = new ArrayList<JMenuItem>();
		specialoptions.add(new CreateBuildingJMenuItem(EnergyExtractionFacility.class,cell,game.getActivePlayer()));
		map.put(TerrainType.ENERGYRESOURCE, specialoptions);
	}
	
	public  List<JMenuItem> getMenuOptions(TerrainType terrain){
		List<JMenuItem> ret = map.get(terrain);
		if(ret == null){
			ret = new ArrayList<JMenuItem>();
			ret.add(new JMenuItem("Not implemented yet."));
			return ret;
		}else{
			return ret;
		}
	}

	
}
