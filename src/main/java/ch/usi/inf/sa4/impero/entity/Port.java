package ch.usi.inf.sa4.impero.entity;

import java.awt.Color;

import ch.usi.inf.sa4.impero.game.Player;

public class Port extends AbstractBuilding {
	public Port(final Player owner) {
		super(owner);
		// indigo 
		this.color = new Color(75,0,130);
		super.health = 100;
		super.name = "Port";
	}
}
