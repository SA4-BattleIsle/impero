package ch.usi.inf.sa4.impero.game;

import java.util.ArrayList;
import java.util.List;

import ch.usi.inf.sa4.impero.action.Action;
import ch.usi.inf.sa4.impero.action.game.BeginTurnAction;
import ch.usi.inf.sa4.impero.action.user.AttackAction;
import ch.usi.inf.sa4.impero.action.user.BuildAction;
import ch.usi.inf.sa4.impero.action.user.ConquerAction;
import ch.usi.inf.sa4.impero.action.user.MoveAction;
import ch.usi.inf.sa4.impero.action.user.UnitCreateAction;
import ch.usi.inf.sa4.impero.entity.AbstractBuilding;
import ch.usi.inf.sa4.impero.entity.AbstractUnit;
import ch.usi.inf.sa4.impero.hexMap.Coordinate;
import ch.usi.inf.sa4.impero.turn.Turn;

/**
 * 
 * @author Luca, Kasim, Jesper, Talal
 * 
 */
public class Player {
	private Turn turn;
	private final String firstName;
	private final String lastName;
	private List<PlayerObserver> observers = new ArrayList<PlayerObserver>();
	private int actionPointsPerTurn;
	private int energy;
	
	public Player(final String firstName, final String lastName, final int actionPointsPerTurn) {
		turn = new Turn(actionPointsPerTurn);
		this.firstName = firstName;
		this.lastName = lastName;
		this.actionPointsPerTurn = actionPointsPerTurn;
	}

	public void addObserver(PlayerObserver observer){
		this.observers.add(observer);
	}
	
	/**
	 * Return the current turn of the player filled with actions
	 * 
	 * @return
	 */
	public Turn getTurn() {
		return this.turn;
	}
	
	public void resetTurn(){
		this.turn = new Turn(this.actionPointsPerTurn);
		this.refreshActionListDisplayRequired();
	}

	private void refreshActionListDisplayRequired(){
		for(PlayerObserver observer : this.observers){
			observer.refreshActionListDisplayRequired();
		}
	}
	
	/**
	 * Add a new Action to the player turn
	 * 
	 * @param a
	 *            Action
	 */
	public void addAction(Action a) {
		this.turn.addAction(a);
		this.refreshActionListDisplayRequired();
	};

	/**
	 * Create and add a new MoveAction to the player turn
	 * 
	 * @param from
	 *            Coordinate from
	 * @param to
	 *            Coordinate to
	 */
	public void createMoveAction(Coordinate from, Coordinate to, Class<? extends AbstractUnit> unit) {
		MoveAction newMA = new MoveAction(from, to, unit, this);
		this.addAction(newMA);
	}

	/**
	 * Create and add a new AttackAction to the player turn
	 * 
	 * @param from
	 *            Coordinate of the Attacker Entity
	 * @param to
	 *            Coordinate of the Enemy Entity
	 */
	public void createAttackAction(Coordinate from, Coordinate to) {
		AttackAction newAA = new AttackAction(from, to, this);
		this.addAction(newAA);
	}

	/**
	 * Create and add a new BuildAction to the player turn
	 * 
	 * @param where
	 *            Coordinate of the newly created building
	 * @param what
	 *            Class of the building
	 */
	public void createBuildAction(Coordinate where,
			Class<? extends AbstractBuilding> what) {
		BuildAction newBA = new BuildAction(where, what, this);
		this.addAction(newBA);
	}

	/**
	 * Create and add a new ConquerAction to the player turn
	 * 
	 * @param from
	 *            Coordinate of the Conquer Entity
	 * @param to
	 *            Coordinate of the Conqueree Entity
	 */
	public void createConquerAction(Coordinate from, Coordinate to) {
		ConquerAction newCA = new ConquerAction(from, to, this);
		this.addAction(newCA);
	}

	/**
	 * Create and add a new UnitAction to the player turn
	 * 
	 * @param fromwhat
	 *            Building that creates the unit
	 * @param what
	 *            Class of the unit to be created
	 */
	public void createUnitAction(Coordinate fromwhat,
			Class<? extends AbstractUnit> what) {
		UnitCreateAction newCU = new UnitCreateAction(fromwhat, what, this);
		this.addAction(newCU);
	}

	/**
	 * Return a string representing the player
	 */
	public String toString() {
		return this.firstName + " " + this.lastName;
	}

	public void setStartingActionPoints(int actionPointsPerTurn) {
		this.turn = new Turn(actionPointsPerTurn);
	}

	/**
	 * @return the firstName
	 */
	public String getFirstName() {
		return firstName;
	}

	/**
	 * @return the lastName
	 */
	public String getLastName() {
		return lastName;
	}

	public void addActionPoints(int bonus) {
		this.turn.setActionPoints(this.turn.getActionPoints()+bonus);
	}
	
	public void addEnergy(int amount) {
		this.energy += amount;
	}
	
	public int getEnergyLevel() {
		return this.energy;
	}
	
}
