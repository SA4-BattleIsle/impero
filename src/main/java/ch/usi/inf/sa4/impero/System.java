package ch.usi.inf.sa4.impero;

import ch.usi.inf.sa4.impero.GUI.GUI;
import ch.usi.inf.sa4.impero.game.Game;
import ch.usi.inf.sa4.impero.hexMap.HexMap;
import ch.usi.inf.sa4.impero.mapGeneration.MapGenerator;

/**
 * @author Jesper
 *
 */

public class System {
	private User user;
	private final MapGenerator mg = new MapGenerator();
	private Game game;
	
	public Game startGame(
			final String firstName, final String lastName, final int mapWidth, final int mapHeight, 
			final int landPercent, final int waterPercent, final int mountainPercent, 
			final int energyPercent, final boolean lakes, final boolean rivers, final int actionPointsPerTurn) {
		
		this.user = new User(firstName, lastName);
		HexMap map = mg.generateMap(mapWidth, mapHeight, landPercent, waterPercent, mountainPercent, energyPercent, lakes, rivers);
		this.game = new Game(actionPointsPerTurn, map, firstName, lastName);
		return this.game;
	}

	public Game getGame() {
		return this.game;
	}
	
	
	/**
	 * @param args
	 */
	public static void main(String[] args) {
		//Schedule a job for the event-dispatching thread:
        //creating and showing this application's GUI.
        javax.swing.SwingUtilities.invokeLater(new Runnable() {
            public void run() {
            	GUI gui = new GUI();
            	gui.createAndShowGUI();
            }
        });
	}
	
	
	
	

}
