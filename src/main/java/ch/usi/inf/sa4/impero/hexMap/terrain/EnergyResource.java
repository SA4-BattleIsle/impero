package ch.usi.inf.sa4.impero.hexMap.terrain;

import java.awt.Color;

/**
 * 
 * @author Jesper
 *
 */
public class EnergyResource extends AbstractTerrain {

	public EnergyResource(){
		super();
		// gold
		this.color = new Color(255,215,0);
	}
	
	@Override
	public Color getColor() {
		
		return this.color;
	}

	@Override
	public String toString() {
		
		return "ER";
	}

}
