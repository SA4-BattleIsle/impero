package ch.usi.inf.sa4.impero.action.cost;

import java.util.HashMap;
import java.util.Map;

import ch.usi.inf.sa4.impero.entity.AbstractBuilding;
import ch.usi.inf.sa4.impero.entity.EnergyExtractionFacility;
import ch.usi.inf.sa4.impero.entity.HeadQuarter;
import ch.usi.inf.sa4.impero.entity.Port;

public class BuildingCostMapper extends CostMapper {

	private static Map<Class<? extends AbstractBuilding>,Integer> map = new HashMap<Class<? extends AbstractBuilding>, Integer>() ;
	private static Map<Class<? extends AbstractBuilding>,Integer> emap = new HashMap<Class<? extends AbstractBuilding>, Integer>() ;
	
	static {
		map.put(HeadQuarter.class, 10);
		map.put(EnergyExtractionFacility.class, 10);
		map.put(Port.class, 10);

		emap.put(HeadQuarter.class, 10);
		emap.put(EnergyExtractionFacility.class, 20);
		emap.put(Port.class, 20);
		System.out.println(emap);
	}
	
	public static int getActionPointCost(Class<? extends AbstractBuilding> what){
		return map.get(what);
	}

	public static int getEnergyCost(Class<? extends AbstractBuilding> what){
		return emap.get(what);
	}

}
