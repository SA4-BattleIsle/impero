package ch.usi.inf.sa4.impero.entity;

import java.awt.Color;

import ch.usi.inf.sa4.impero.game.Player;

/**
 * @author 
 *
 */

public class HeadQuarter extends AbstractBuilding {
	
	
	public HeadQuarter(final Player owner) {
		super(owner);
		// slate gray
		this.color = new Color(112,128,144);
		super.health = 200;
		super.name = "HeadQuarter";
	}

}
