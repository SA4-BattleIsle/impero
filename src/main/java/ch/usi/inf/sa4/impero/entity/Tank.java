package ch.usi.inf.sa4.impero.entity;

import java.awt.Color;

import ch.usi.inf.sa4.impero.game.Player;

/**
 * @author Talal
 *
 */

public class Tank extends AbstractGroundUnit {
	public Tank(final Player owner) {
		super(owner);
		super.health = 20;
		super.damage = 100;
		super.name = "Tank";
		
		// saddle brown
		this.color = new Color(139,69,19);
	}
	
}
