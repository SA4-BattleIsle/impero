package ch.usi.inf.sa4.impero.entity;

import java.awt.Color;
import java.awt.Rectangle;
import java.awt.Shape;

import ch.usi.inf.sa4.impero.game.Player;

/**
 * @author 
 *
 */

public interface Entity {

	 public Player getOwner();
	 public int getHealthStatus();
	 public String getName();
	 public void reciveDamage(int damage);
	 
	 // methods for drawing
	 public void setCenter(final int x,final int y);
	 public Shape getShape();
	 public void setWidth(int width);
	 public Color getColor();
	 public void generateShape();

}
