package ch.usi.inf.sa4.impero.hexMap.terrain;

/**
 * This enumerator represents the type of terrains.
 * 
 * @author Luca
 *
 */
public enum TerrainType {
	DEEPSEA,
	SEA,
	SAND,
	FIELD,
	FOREST,
	MOUNTAIN,
	ENERGYRESOURCE;
}
