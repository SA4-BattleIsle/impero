package ch.usi.inf.sa4.impero.turn;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.LinkedList;
import java.util.List;

import ch.usi.inf.sa4.impero.action.Action;
import ch.usi.inf.sa4.impero.action.ActionSequence;
import ch.usi.inf.sa4.impero.action.ActionValidator;
import ch.usi.inf.sa4.impero.game.Game;

/**
 * 
 * @author Kasim
 * 
 */
public class TurnMerger {

	private List<Turn> turns = new ArrayList<Turn>();

	public TurnMerger() {

	}

	public void addTurn(Turn turn) {
		this.turns.add(turn);
	}

	/**
	 * Merges the list of actions in each turn according to end times (less
	 * cost) Since there could be two actions from two turns that have same cost
	 * they are saved into the ActionSequence object. The ActionSequence list is
	 * the actual sequence of actions to be executed in the correct order.
	 * 
	 * @return List of ActionSequence containing all the merged actions
	 */

	public List<ActionSequence> merge(Game g) {
		boolean done = false;
		int cursequence = 1; // Sequence 0 is reserved for system stuff
		int curActionPoinst = -1;
		List<Action> current = new LinkedList<Action>();
		List<ActionSequence> sequence = new ArrayList<ActionSequence>();

		while (!done) {
			done = true;
			for (Turn t : this.turns) {
				if (t.size() > 0) {
					Action tbe = t.pollAction();
					// Fill the stack with stuff
					current.add(tbe);
					done &= false;
				} else {
					done &= true;
				}
			}
			// Sort the stack by cost
			Collections.sort(current, new Comparator<Action>() {
				public int compare(Action a, Action b) {
					return a.getActionPointCost() - b.getActionPointCost();
				}
			});
			// Group stuff and create ActionSequences
			ActionSequence as = new ActionSequence(cursequence);

			for (Action a : current) {
				if (curActionPoinst >= 0
						&& curActionPoinst != a.getActionPointCost()) {
					// Close the sequence and and to the sequence list
					sequence.add(as);
					cursequence++;
					as = new ActionSequence(cursequence);
				}
				as.addAction(a);
				curActionPoinst = a.getActionPointCost();
			}
			// Add last sequence to sequence list
			sequence.add(as);
			current = new LinkedList<Action>();
			cursequence++;
		}
		
		//Validates the final lists of sequences against the ActionValidator
		ActionValidator av = new ActionValidator(sequence, g.getMap(),g);
		return av.validate();
	}

}
