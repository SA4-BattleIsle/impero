package ch.usi.inf.sa4.impero.action;

import ch.usi.inf.sa4.impero.game.Game;

public interface Action {

	public void execute(Game game);

	public int getActionPointCost();

	public int getEnergyCost();

	public void setEnabled(boolean status);

	public boolean getEnabled();


}
