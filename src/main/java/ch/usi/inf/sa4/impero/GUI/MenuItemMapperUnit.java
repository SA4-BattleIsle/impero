package ch.usi.inf.sa4.impero.GUI;

import java.lang.reflect.Modifier;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.swing.JMenuItem;

import org.reflections.Reflections;

import ch.usi.inf.sa4.impero.entity.AbstractBuilding;
import ch.usi.inf.sa4.impero.entity.AbstractGroundUnit;
import ch.usi.inf.sa4.impero.entity.AbstractNavalUnit;
import ch.usi.inf.sa4.impero.entity.AbstractUnit;
import ch.usi.inf.sa4.impero.entity.Entity;
import ch.usi.inf.sa4.impero.entity.HeadQuarter;
import ch.usi.inf.sa4.impero.entity.Port;
import ch.usi.inf.sa4.impero.game.Game;
import ch.usi.inf.sa4.impero.hexMap.Coordinate;

public class MenuItemMapperUnit {
	
		private  Map<Class<? extends AbstractBuilding>,List<JMenuItem> > map = new HashMap<Class<? extends AbstractBuilding>, List<JMenuItem>>() ;
		
		public MenuItemMapperUnit(Game game, Coordinate cell) {
			//HeadQuarter context menu options
			List<JMenuItem> hqoptions = new ArrayList<JMenuItem>();
			
			//Add all the buildings using reflection (since its hq)
			Reflections reflections = new Reflections("ch.usi.inf.sa4.impero.entity");
			Set<Class<? extends AbstractGroundUnit>> subclasses = reflections.getSubTypesOf(AbstractGroundUnit.class);
					
			for(Class<? extends AbstractGroundUnit> c : subclasses){
				if(!Modifier.isAbstract(c.getModifiers())){
					hqoptions.add(new CreateUnitJMenuItem(c, cell , game.getActivePlayer()));
				}
			}
			System.out.println("HQO:"+hqoptions.size());
			map.put(HeadQuarter.class, hqoptions);
			
			//Port context menu options
			List<JMenuItem> portoptions = new ArrayList<JMenuItem>();
			//Add all the buildings using reflection (since its hq)
			reflections = new Reflections("ch.usi.inf.sa4.impero.entity");
			Set<Class<? extends AbstractNavalUnit>> subclassesnaval = reflections.getSubTypesOf(AbstractNavalUnit.class);
					
			for(Class<? extends AbstractNavalUnit> c : subclassesnaval){
				if(!Modifier.isAbstract(c.getModifiers())){
					portoptions.add(new CreateUnitJMenuItem(c, cell , game.getActivePlayer()));
				}
			}
			map.put(Port.class, portoptions);
		}
		
		public  List<JMenuItem> getMenuOptions(Class<? extends Entity> what){
			List<JMenuItem> ret = map.get(what);
			if(ret == null){
				ret = new ArrayList<JMenuItem>();
				ret.add(new JMenuItem("Not implemented yet."));
				return ret;
			}else{
				return ret;
			}
		}
}
