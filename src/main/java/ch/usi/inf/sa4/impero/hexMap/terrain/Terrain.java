package ch.usi.inf.sa4.impero.hexMap.terrain;

import java.awt.Color;
import java.awt.Polygon;

public interface Terrain {
	public void setCenter(final int x,final int y);
	public Polygon getPolygon();
	public void setRadius(int radius);
	public Color getColor();
}
