package ch.usi.inf.sa4.impero.action.user;

import ch.usi.inf.sa4.impero.game.Game;
import ch.usi.inf.sa4.impero.game.Player;
import ch.usi.inf.sa4.impero.hexMap.Coordinate;

/**
 * @author 
 *
 */

public class ConquerAction extends AbstractUserAction {
	private final Coordinate from;
	private final Coordinate to;
	
	public ConquerAction(Coordinate from, Coordinate to, Player owner) {
		super(owner, from);
		this.from = from;
		this.to = to;
	}

	@Override
	public void execute(Game game) {
		game.logEvent("Execute: "+ this.toString());
		// TODO Auto-generated method stub
		
	}

	@Override
	public int getActionPointCost() {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public int getEnergyCost() {
		// TODO Auto-generated method stub
		return 0;
	}

}
