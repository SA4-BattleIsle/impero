package ch.usi.inf.sa4.impero.hexMap.terrain;

import java.awt.Color;

/**
 * @author Kasim and Jesper
 *
 */
public class Mountain extends AbstractTerrain {
	public Mountain(){
		// slate gray
		this.color = new Color(112,128,144);
	}
	
	/* (non-Javadoc)
	 * @see terrain.Terrain#getColor()
	 */
	@Override
	public Color getColor() {
		return this.color;
	}
	
	/* (non-Javadoc)
	 * @see terrain.Terrain#toString()
	 */
	@Override
	public String toString() {
		return "G";
	}
	
}