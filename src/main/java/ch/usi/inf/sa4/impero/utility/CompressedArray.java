package ch.usi.inf.sa4.impero.utility;

import java.util.ArrayList;

/**
 * testing
 * 
 * @author admin
 *
 */
public class CompressedArray extends ArrayList<Integer> {

	/**
	 * 
	 */
	private static final long serialVersionUID = -8577164926163323380L;
	
	public CompressedArray() {
		
	}
	
	public CompressedArray(int initSize) {
		this.ensureCapacity(initSize);
		for (int i = 0; i < initSize; i++) {
			this.add(0);
		}
	}
	
	

}
