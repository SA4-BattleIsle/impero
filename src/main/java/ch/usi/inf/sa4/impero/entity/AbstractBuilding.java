package ch.usi.inf.sa4.impero.entity;

import java.awt.Polygon;
import java.awt.Rectangle;

import ch.usi.inf.sa4.impero.game.Player;

/**
 * @author 
 *
 */

public abstract class AbstractBuilding extends AbstractEntity {
	
	public AbstractBuilding(Player owner) {
		super(owner);
	}

	
	/* (non-Javadoc)
	 * @see ch.usi.inf.sa4.impero.entity.Entity#generateShape()
	 */
	public void generateShape() {		
		this.shape = new Rectangle(this.x, this.y, this.width, this.width);
	}

}
