package ch.usi.inf.sa4.impero.GUI;

import java.awt.Color;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.List;

import javax.swing.BoxLayout;
import javax.swing.DefaultListModel;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.JScrollPane;
import javax.swing.ScrollPaneConstants;
import javax.swing.SwingUtilities;

import ch.usi.inf.sa4.impero.action.Action;
import ch.usi.inf.sa4.impero.action.user.AbstractUserAction;
import ch.usi.inf.sa4.impero.game.Game;
import ch.usi.inf.sa4.impero.game.GameObserver;
import ch.usi.inf.sa4.impero.game.Player;
import ch.usi.inf.sa4.impero.game.PlayerObserver;

public class TurnPanel extends JPanel implements PlayerObserver, GameObserver{

	/**
	 * @author jesper
	 */
	private static final long serialVersionUID = -28622307120068304L;
	private JPanel turn = new JPanel();
	private DefaultListModel<String> model = new DefaultListModel<String>();
	private JList<String> turnList = new JList<String>(this.model);
	private final Game game;
	private JScrollPane scroller;
	private List<Action> actionList;
	
	public TurnPanel(int width, final GamePanel parent) {
		this.game = parent.getGame();
		
		JLabel label = new JLabel("TURN");
		
		this.setLayout(new BoxLayout(this, BoxLayout.Y_AXIS));
		this.add(label);
		
		this.turn.setBackground(Color.white);
		
		this.model.addElement("Empty list");
		this.turnList.setModel(model);
	
		this.turn.add(this.turnList);
	
		// wrap everything in a scrollpane
		this.scroller = new JScrollPane(this.turn);
		this.scroller.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);

		this.setLayout(new BoxLayout(this, BoxLayout.Y_AXIS));
		this.add(label);
		this.add(this.scroller);
		
//		this.add(turn);
		
		// observe game changes and update
		game.addObserver(this);
		
		// add click functionality
		
		this.turnList.addMouseListener(new MouseAdapter() {
			
		    public void mouseClicked(MouseEvent e) {
		    	
		    	JList<String> turnList = (JList<String>)e.getSource();
		    	
		    	final int index = turnList.locationToIndex(e.getPoint());
		    	AbstractUserAction action = (AbstractUserAction) actionList.get(index);
		        parent.centerMapAtCoordinate(action.getLocation());
		    	
		           
		   		if (SwingUtilities.isRightMouseButton(e)) {
				     e.consume();
				     //handle right click event.
				     
				     JPopupMenu menu = new JPopupMenu("actions");
				     JMenuItem removeItem = new JMenuItem("remove action");
				     JMenuItem moveUpItem = new JMenuItem("move up");
				     JMenuItem moveDownItem = new JMenuItem("move down");
				     
				     menu.add(removeItem);
				     menu.add(moveUpItem);
				     menu.add(moveDownItem);
				     
				     // add mouse events
				     removeItem.addMouseListener(new MouseAdapter() {
				    	 
				    	@Override
						public void mouseReleased(MouseEvent e) {
				    		 				    		
				    		actionList.remove(index);
				    		refreshActionListDisplayRequired();
				    		
				    	 }     
				     });
				     
				     moveUpItem.addMouseListener(new MouseAdapter() {
				    	 
					    	@Override
							public void mouseReleased(MouseEvent e) {
					    		// can move if not beginning of list
					    		if (index > 0) {
					    			
					    			// make a swap up
					    			Action temp = actionList.get(index-1);
					    			actionList.set(index-1, actionList.get(index));
					    			actionList.set(index, temp);
					    			refreshActionListDisplayRequired();
					    		}
					    	 }     
					     });
				     
				     moveDownItem.addMouseListener(new MouseAdapter() {
				    	 
					    	@Override
							public void mouseReleased(MouseEvent e) {
					    		 
					    		// can move if not end of list
					    		if (index < actionList.size()-1) {
					    			
					    			// make a swap down
					    			Action temp = actionList.get(index+1);
					    			actionList.set(index+1, actionList.get(index));
					    			actionList.set(index, temp);
					    			refreshActionListDisplayRequired();
					    		} 
					    	 }     
					     });
				     
				     menu.show(e.getComponent(), e.getPoint().x + 10, e.getPoint().y);
				         
				}
	           
		    }
		});

	}

	@Override
	public void refreshActionListDisplayRequired() {
	
		// update list for selected player
		Player player = game.getActivePlayer();
		this.actionList = player.getTurn().getActions();		
		this.model.clear();
		
		for(Action a: actionList){
			this.model.addElement(a.toString());
		}
	}

	@Override
	public void refreshMapDisplayRequired() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void refreshEntityListDisplayRequired() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void logEvent(String text) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void gameStarted() {
		
		// add observer to players
		for (Player p : game.getPlayers()) {
			p.addObserver(this);
		}
		
	}

	@Override
	public void activePlayerChanged() {
		
		refreshActionListDisplayRequired();
		
	}
}
