package ch.usi.inf.sa4.impero.game;

/**\
 * Observer interface for Game class
 * 
 * @author Kasim
 *
 */

public interface GameObserver {
	
	public void refreshMapDisplayRequired();
	
	public void refreshEntityListDisplayRequired();	
	
	public void logEvent(String text);
	
	public void gameStarted();
	
	public void activePlayerChanged();
	
}
