package ch.usi.inf.sa4.impero.GUI;

import java.awt.Color;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.List;

import javax.swing.BoxLayout;
import javax.swing.DefaultListModel;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.ScrollPaneConstants;

import ch.usi.inf.sa4.impero.entity.Entity;
import ch.usi.inf.sa4.impero.game.Game;
import ch.usi.inf.sa4.impero.game.GameObserver;
import ch.usi.inf.sa4.impero.game.Player;
import ch.usi.inf.sa4.impero.game.PlayerObserver;
import ch.usi.inf.sa4.impero.hexMap.Coordinate;
import ch.usi.inf.sa4.impero.hexMap.HexMap;

public class ArmyPanel extends JPanel implements GameObserver, PlayerObserver{

	/**
	 * @author Kasim
	 * @author Talal
	 * @author Jesper
	 */
	private static final long serialVersionUID = 7292459821653312353L;
	private JPanel armyPanel = new JPanel();
	private JScrollPane scroller;
	private DefaultListModel model = new DefaultListModel();
	private JList armyList = new JList(this.model);
	private Game game;
	private HexMap map;
	private GamePanel parentPanel;
	private List<Entity> playerArmyList;
	
	public ArmyPanel(int width, GamePanel parentPanel) {
		
		this.game = parentPanel.getGame();	
		this.map = game.getMap();
		this.parentPanel = parentPanel;
		JLabel label = new JLabel("ARMY");
		
		this.setLayout(new BoxLayout(this, BoxLayout.Y_AXIS));
		this.add(label);
		
		this.armyPanel.setBackground(Color.white);
		this.model.addElement("Empty list");
		this.armyList.setModel(model);
		this.armyPanel.add(this.armyList);
		
		// wrap everything in a scrollpane
		this.scroller = new JScrollPane(this.armyPanel);
		this.scroller.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
		this.setLayout(new BoxLayout(this, BoxLayout.Y_AXIS));
		this.add(label);
		this.add(this.scroller);
		
		this.add(scroller);
		
		// listen to changes in game
		game.addObserver(this);
		
		
		// add click functionality
		this.armyList.addMouseListener(new MouseAdapter() {
		    public void mouseClicked(MouseEvent evt) {
		        JList armyList = (JList)evt.getSource();
		           int index = armyList.locationToIndex(evt.getPoint());
		           Entity entity = playerArmyList.get(index);
		           Coordinate entityCoordinate = map.getCoordinatesByEntity(entity);
		           centerEntity(entityCoordinate);
//		           
		    }
		});
	}
	/*
	 * center the map to the entity coordinate
	 * */
	public void centerEntity (Coordinate coordinate){
        parentPanel.centerMapAtCoordinate(coordinate);
	
}
		
	@Override
	public void refreshActionListDisplayRequired() {
		// TODO Auto-generated method stub
		
	}
	@Override
	public void refreshMapDisplayRequired() {
		// TODO Auto-generated method stub
		
	}
	@Override
	public void refreshEntityListDisplayRequired() {
		// TODO Auto-generated method stub
		this.armyPanel = new JPanel();
		HexMap map = game.getMap();
		
		this.playerArmyList = map.getArmySummary(game.getActivePlayer());
		this.model.clear();
		
		for(Entity e: playerArmyList){
			this.model.addElement(e.getName() + " " + e.getHealthStatus());				
		}
		 		
	}
        	
		
	@Override
	public void logEvent(String text) {
		// TODO Auto-generated method stub
		
	}
	@Override
	public void gameStarted() {
		
		// add observer to players
		for (Player p : game.getPlayers()) {
			p.addObserver(this);
		}
		
	}
	@Override
	public void activePlayerChanged() {
		
		this.repaint();
		
	}
	

}
