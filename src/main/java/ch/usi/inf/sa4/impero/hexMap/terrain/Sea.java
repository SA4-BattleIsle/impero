package ch.usi.inf.sa4.impero.hexMap.terrain;

import java.awt.Color;

/**
 * @author Kasim and Jesper
 *
 */
public class Sea extends AbstractTerrain {
	public Sea(){
		super();
		// steel blue
		this.color = new Color(70,130,180);
	}
	
	/* (non-Javadoc)
	 * @see terrain.Terrain#getColor()
	 */
	@Override
	public Color getColor() {
		return this.color;
	}



	/* (non-Javadoc)
	 * @see terrain.Terrain#toString()
	 */
	@Override
	public String toString() {
		return "W";
	}

}
