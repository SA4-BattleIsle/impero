package ch.usi.inf.sa4.impero.action.cost;

import java.util.HashMap;
import java.util.Map;

import ch.usi.inf.sa4.impero.entity.AbstractUnit;
import ch.usi.inf.sa4.impero.entity.Airplane;
import ch.usi.inf.sa4.impero.entity.Ship;
import ch.usi.inf.sa4.impero.entity.Soldier;
import ch.usi.inf.sa4.impero.entity.Tank;
import ch.usi.inf.sa4.impero.hexMap.terrain.TerrainType;

public class MovementCostMapper extends CostMapper{
	private static Map<TerrainType,Map<Class<? extends AbstractUnit>, Integer>> map = new HashMap<TerrainType,Map<Class<? extends AbstractUnit>, Integer>>() ;
	
	static {
		//cost for deep sea cells
		Map<Class<? extends AbstractUnit>, Integer> deepSeaUnitCost= new HashMap<Class<? extends AbstractUnit>, Integer>();
		deepSeaUnitCost.put(Airplane.class, 1);
		deepSeaUnitCost.put(Ship.class, 1);
		deepSeaUnitCost.put(Soldier.class, -1);
		deepSeaUnitCost.put(Tank.class, -1);
		map.put(TerrainType.DEEPSEA, deepSeaUnitCost);
				
		//cost for sea cells
		Map<Class<? extends AbstractUnit>, Integer> waterUnitCost= new HashMap<Class<? extends AbstractUnit>, Integer>();
		waterUnitCost.put(Airplane.class, 1);
		waterUnitCost.put(Ship.class, 1);
		waterUnitCost.put(Soldier.class, -1);
		waterUnitCost.put(Tank.class, -1);
		map.put(TerrainType.SEA, waterUnitCost);
		
		//cost for sand cells
		Map<Class<? extends AbstractUnit>, Integer> groundUnitCost= new HashMap<Class<? extends AbstractUnit>, Integer>();
		groundUnitCost.put(Airplane.class, 1);
		groundUnitCost.put(Ship.class, -1);
		groundUnitCost.put(Soldier.class, 2);
		groundUnitCost.put(Tank.class, 1);
		map.put(TerrainType.SAND, groundUnitCost);
		
		//cost for field cells
		Map<Class<? extends AbstractUnit>, Integer> fieldUnitCost= new HashMap<Class<? extends AbstractUnit>, Integer>();
		fieldUnitCost.put(Airplane.class, 1);
		fieldUnitCost.put(Ship.class, -1);
		fieldUnitCost.put(Soldier.class, 3);
		fieldUnitCost.put(Tank.class, 2);
		map.put(TerrainType.FIELD, fieldUnitCost);
		
		//cost for forest cells
		Map<Class<? extends AbstractUnit>, Integer> forestUnitCost= new HashMap<Class<? extends AbstractUnit>, Integer>();
		forestUnitCost.put(Airplane.class, 1);
		forestUnitCost.put(Ship.class, -1);
		forestUnitCost.put(Soldier.class, 4);
		forestUnitCost.put(Tank.class, 5);
		map.put(TerrainType.FOREST, forestUnitCost);
		
		//cost for mountain cells
		Map<Class<? extends AbstractUnit>, Integer> mountainUnitCost= new HashMap<Class<? extends AbstractUnit>, Integer>();
		mountainUnitCost.put(Airplane.class, 1);
		mountainUnitCost.put(Ship.class, -1);
		mountainUnitCost.put(Soldier.class, 5);
		mountainUnitCost.put(Tank.class, -1);
		map.put(TerrainType.MOUNTAIN, mountainUnitCost);
		
		//cost for energy source cells
		Map<Class<? extends AbstractUnit>, Integer> energySourceUnitCost = new HashMap<Class<? extends AbstractUnit>, Integer>();
		energySourceUnitCost.put(Airplane.class, -1);
		energySourceUnitCost.put(Ship.class, -1);
		energySourceUnitCost.put(Soldier.class, -1);
		energySourceUnitCost.put(Tank.class, -1);
		map.put(TerrainType.ENERGYRESOURCE, mountainUnitCost);
		
		
	}
	
	public static int getActionPointCost(Class<? extends AbstractUnit> what, TerrainType type){
		return map.get(type).get(what);
	}

	
}
