package ch.usi.inf.sa4.impero.action.user;

import java.util.List;

import ch.usi.inf.sa4.impero.action.cost.MovementCostMapper;
import ch.usi.inf.sa4.impero.entity.AbstractUnit;
import ch.usi.inf.sa4.impero.game.Game;
import ch.usi.inf.sa4.impero.game.Player;
import ch.usi.inf.sa4.impero.hexMap.Coordinate;
import ch.usi.inf.sa4.impero.hexMap.HexMap;
import ch.usi.inf.sa4.impero.utility.MapOperations;

/**
 * @author Kasim
 *
 */

public class MoveAction extends AbstractUserAction {
	private final Coordinate from;
	private final Coordinate to;
	private final Class<? extends AbstractUnit> what;
	private List<Coordinate> path;
	private int cost;
	private Player owner;
	
	public MoveAction(Coordinate from, Coordinate to, Class<? extends AbstractUnit> what, Player owner){
		super(owner, from);
		this.from = from;
		this.to = to;
		this.what = what;
		this.cost = 0;
		this.owner = owner;
	}
	
	public Player getOwner(){
		return this.owner;
	}
	
	@Override
	public void execute(Game game) {
		game.logEvent("Execute: "+ this.toString());
		HexMap map = game.getMap();
		path = MapOperations.getShortestPath(from, to, map, owner.getTurn(), what);
		for (Coordinate coordinate : path) {
			cost += MovementCostMapper.getActionPointCost(what, game.getMap().getTerrainType(coordinate));
			}
		
		map.moveEntity(from, to);
		game.refreshMapDisplayRequired();
		game.refreshEntityListDisplayRequired();
	}

	@Override
	public int getActionPointCost() {
		
		return this.cost;
	}

	@Override
	public int getEnergyCost() {
		return 0;
	}
	
	public Coordinate getFrom(){
		return this.from;
	}
	
	public Coordinate getTo(){
		return this.to;
	}

	public String toString(){
		return "Unit @" + this.from + " moves to " + this.to;
	}
}
