package ch.usi.inf.sa4.impero.entity;

import java.awt.Color;
import java.awt.Polygon;
import java.awt.Shape;

import ch.usi.inf.sa4.impero.game.Player;

/**
 * @author Talal
 * @author Jesper
 *
 */

public abstract class AbstractEntity implements Entity {

	private Player owner;
	protected int health;
	
	// used for drawing
	protected int x;
	protected int y;
	protected Color color;
	protected Shape shape;
	public int width = 3;
	public String name = "AbstractEntity";
	
	public AbstractEntity(final Player owner) {
		this.x = 0;
		this.y = 0;
		this.generateShape();
		this.owner = owner;
	}

	/* (non-Javadoc)
	 * @see ch.usi.inf.sa4.impero.entity.Entity#setCenter(int, int)
	 */
	@Override
	public void setCenter(int x, int y) {
		this.x = x - (this.width / 2);
		this.y = y - (this.width / 2);
		this.generateShape();
	}

	/* (non-Javadoc)
	 * @see ch.usi.inf.sa4.impero.entity.Entity#getRectangle()
	 */
	@Override
	public Shape getShape() {
		return this.shape;
	}

	/* (non-Javadoc)
	 * @see ch.usi.inf.sa4.impero.entity.Entity#setWidth(int)
	 */
	@Override
	public void setWidth(int width) {	
		this.width = width;
	}

	/* (non-Javadoc)
	 * @see ch.usi.inf.sa4.impero.entity.Entity#getColor()
	 */
	@Override
	public Color getColor() {
		return this.color;
	}
	
	/**
	 * Returns the owner .
	 * @return  Player.
	 */
	@Override
	public Player getOwner() {
		return this.owner;
	}

	@Override
	/**
	 *get health .
	 * @return  heath.
	 */
	
	public int getHealthStatus(){
		return this.health;
	}
	
	@Override
	 /**
	 * received damage from the enemy unit and increase the health.
	 * @param damage received
	 */
	public void reciveDamage(int damage){
	 this.health -= damage;
 }
	 public String getName(){
		 return name;
	 };
	
}
