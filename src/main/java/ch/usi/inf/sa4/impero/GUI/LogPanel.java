package ch.usi.inf.sa4.impero.GUI;

import java.awt.Dimension;

import javax.swing.BoxLayout;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollBar;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.ScrollPaneConstants;

public class LogPanel extends JPanel {

	/**
	 * @author jesper
	 */
	private static final long serialVersionUID = -102954581697903438L;
	
	private final JTextArea log;
	private final JScrollPane scroller;
	
	public LogPanel(int width) {
		
		this.setPreferredSize(new Dimension(width, 250));
		
		log = new JTextArea(0, 1);
		log.setEditable(false);
		log.setLineWrap(true);
		
		JLabel label = new JLabel("LOG");
		
		this.scroller = new JScrollPane(log);
		this.scroller.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);

		this.setLayout(new BoxLayout(this, BoxLayout.Y_AXIS));
		this.add(label);
		this.add(this.scroller);
				
	}
	
	public void appendLogString(String logString) {
		
		if (!logString.endsWith("\n"))
			logString += "\n";
		
		log.append(logString);
		
		// scroll to the bottom
		JScrollBar scrollBar = this.scroller.getVerticalScrollBar();
		scrollBar.setValue(scrollBar.getMaximum());
	
	}
	
	

}
