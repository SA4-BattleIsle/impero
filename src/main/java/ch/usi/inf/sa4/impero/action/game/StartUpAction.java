package ch.usi.inf.sa4.impero.action.game;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import ch.usi.inf.sa4.impero.action.user.BuildStartingHeadQuartersAction;
import ch.usi.inf.sa4.impero.entity.HeadQuarter;
import ch.usi.inf.sa4.impero.game.Game;
import ch.usi.inf.sa4.impero.hexMap.Coordinate;
import ch.usi.inf.sa4.impero.hexMap.HexMap;
import ch.usi.inf.sa4.impero.hexMap.terrain.TerrainType;

/**
 * This class represent a start up action. The purpose of this action is to place the initial headquarters in a way they are equidistant. 
 * For instance the headquarters are placed randomly on a ground cell.
 * 
 * @author Luca 
 *
 */
public class StartUpAction extends AbstractGameAction {

	@Override
	public void execute(Game game) {
		List<Coordinate> coordinates = generateHeadQuartersCoordinates(game);
		for (int i = 0; i < coordinates.size(); i++) {
			game.addAction(new BuildStartingHeadQuartersAction(
					coordinates.get(i), HeadQuarter.class, game.getPlayerByIndex(i)));
		}
	}
	
	/**
	 * Generate equidistant coordinates on the map to place the initial headquarters.
	 * (For now the coordinates are chosen randomly)
	 * 
	 * @param game
	 * @return List of coordinates
	 */
	private List<Coordinate> generateHeadQuartersCoordinates(Game game) {
		List<Coordinate> coordinates = new ArrayList<Coordinate>();
		int numberOfPlayers = game.getNumberOfPlayers();
		HexMap map = game.getMap();
		
		for (int i = 0; i < numberOfPlayers; i++) {
			Random rand = new Random();
			int x = rand.nextInt(map.getWidth()-1);
			int y = rand.nextInt(map.getHeight()-1);
			//Seek until some ground is found
			Coordinate coordinate = new Coordinate(x, y);
			while(map.checkTerrainType(coordinate, TerrainType.SEA) || map.checkTerrainType(coordinate, TerrainType.DEEPSEA) || checkCoordinate(coordinates, coordinate)) {
				x = rand.nextInt(map.getWidth()-1);
				y = rand.nextInt(map.getHeight()-1);
				coordinate = new Coordinate(x, y);
			}
			coordinates.add(coordinate);
		}
		return coordinates;
	}
	
	/**
	 * Internal method to test if the chosen coordinate is already in the coordinates list.
	 * 
	 * @param coordinates
	 * @param currentCoordinate
	 * @return true if the given coordinate is in the given list, false otherwise
	 */
	private boolean checkCoordinate(List<Coordinate> coordinates, Coordinate currentCoordinate) {
		for (Coordinate coordinate : coordinates) {
			if(coordinate.getX() == currentCoordinate.getX() && coordinate.getY() == currentCoordinate.getY()) {
				return true;
			}
		}
		return false;
	}

	@Override
	public int getActionPointCost() {
		return 0;
	}

	@Override
	public int getEnergyCost() {
		// TODO Auto-generated method stub
		return 0;
	}
}
