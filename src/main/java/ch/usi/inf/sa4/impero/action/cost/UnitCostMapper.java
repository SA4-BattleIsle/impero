package ch.usi.inf.sa4.impero.action.cost;

import java.util.HashMap;
import java.util.Map;

import ch.usi.inf.sa4.impero.entity.AbstractUnit;
import ch.usi.inf.sa4.impero.entity.Airplane;
import ch.usi.inf.sa4.impero.entity.Ship;
import ch.usi.inf.sa4.impero.entity.Soldier;
import ch.usi.inf.sa4.impero.entity.Tank;

public class UnitCostMapper extends CostMapper {
	static Map<Class<? extends AbstractUnit>,Integer> actionPointMapping = new HashMap<Class<? extends AbstractUnit>, Integer>() ;
	static Map<Class<? extends AbstractUnit>,Integer> energyMapping = new HashMap<Class<? extends AbstractUnit>, Integer>() ;
	
	static{
		actionPointMapping.put(Soldier.class, 1);
		actionPointMapping.put(Tank.class, 5);
		actionPointMapping.put(Airplane.class, 10);
		actionPointMapping.put(Ship.class, 100);
		
		energyMapping.put(Soldier.class, 1);
		energyMapping.put(Tank.class, 5);
		energyMapping.put(Airplane.class, 10);
		energyMapping.put(Ship.class, 100);
	}

	public static int getActionPointCost(Class<? extends AbstractUnit> what) {
		return actionPointMapping.get(what);
	}

	public static int getEnergyCost(Class<? extends AbstractUnit> what) {
		return energyMapping.get(what);
	}

}
