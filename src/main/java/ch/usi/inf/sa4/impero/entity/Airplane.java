package ch.usi.inf.sa4.impero.entity;

import java.awt.Color;

import ch.usi.inf.sa4.impero.game.Player;

/**
 * @author Talal
 *
 */


public class Airplane extends AbstractAirForceUnit {
	public Airplane(final Player owner) {
		super(owner);
		super.health = 30;
		super.damage = 20;
		super.name = "Airplane";
		
		// sky blue
		this.color = new Color(135,206,235);
	}
	
	
}
