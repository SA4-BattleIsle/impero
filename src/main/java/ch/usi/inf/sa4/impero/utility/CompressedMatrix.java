package ch.usi.inf.sa4.impero.utility;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.ListIterator;

import ch.usi.inf.sa4.impero.hexMap.Coordinate;
import ch.usi.inf.sa4.impero.hexMap.terrain.TerrainType;

/**
 * CompressedMatrix is a data structure that optimizes the storing of a matrix by using
 * CompressedArray as rows.
 * 
 * @author jesper
 *
 */
public class CompressedMatrix implements List<CompressedArray> {
	
	private List<CompressedArray> map = new ArrayList<>();
	
	public CompressedMatrix() {};
	
	/**
	 * initialize the matrix with 0s
	 * 
	 * @param columns
	 * @param rows
	 */
	public CompressedMatrix(int columns, int rows) {
		
		for (int i = 0; i < rows; i++) {
			map.add(new CompressedArray(columns));
		}
	}

	/* (non-Javadoc)
	 * @see java.util.List#size()
	 */
	@Override
	public int size() {
		return map.size();
	}

	/* (non-Javadoc)
	 * @see java.util.List#isEmpty()
	 */
	@Override
	public boolean isEmpty() {
		
		return map.isEmpty();
	}

	/* (non-Javadoc)
	 * @see java.util.List#contains(java.lang.Object)
	 */
	@Override
	public boolean contains(Object o) {
		return map.contains(o);
	}

	/* (non-Javadoc)
	 * @see java.util.List#iterator()
	 */
	@Override
	public Iterator<CompressedArray> iterator() {
		return map.iterator();
	}

	/* (non-Javadoc)
	 * @see java.util.List#toArray()
	 */
	@Override
	public Object[] toArray() {
		
		return map.toArray();
	}

	/* (non-Javadoc)
	 * @see java.util.List#toArray(java.lang.Object[])
	 */
	@Override
	public <T> T[] toArray(T[] a) {
		
		return map.toArray(a);
	}

	/* (non-Javadoc)
	 * @see java.util.List#add(java.lang.Object)
	 */
	@Override
	public boolean add(CompressedArray e) {

		return map.add(e);
	}

	/* (non-Javadoc)
	 * @see java.util.List#remove(java.lang.Object)
	 */
	@Override
	public boolean remove(Object o) {
		
		return map.remove(o);
	}

	/* (non-Javadoc)
	 * @see java.util.List#containsAll(java.util.Collection)
	 */
	@Override
	public boolean containsAll(Collection<?> c) {
		
		return map.containsAll(c);
	}

	/* (non-Javadoc)
	 * @see java.util.List#addAll(java.util.Collection)
	 */
	@Override
	public boolean addAll(Collection<? extends CompressedArray> c) {

		return map.addAll(c);
	}

	/* (non-Javadoc)
	 * @see java.util.List#removeAll(java.util.Collection)
	 */
	@Override
	public boolean removeAll(Collection<?> c) {
		
		return map.removeAll(c);
	}

	/* (non-Javadoc)
	 * @see java.util.List#retainAll(java.util.Collection)
	 */
	@Override
	public boolean retainAll(Collection<?> c) {
		
		return map.retainAll(c);
	}

	/* (non-Javadoc)
	 * @see java.util.List#clear()
	 */
	@Override
	public void clear() {
		
		map.clear();
	}

	/* (non-Javadoc)
	 * @see java.util.List#addAll(int, java.util.Collection)
	 */
	@Override
	public boolean addAll(int index, Collection<? extends CompressedArray> c) {
		
		return map.addAll(c);
	}

	/* (non-Javadoc)
	 * @see java.util.List#get(int)
	 */
	@Override
	public CompressedArray get(int index) {
		
		return map.get(index);
	}

	/* (non-Javadoc)
	 * @see java.util.List#set(int, java.lang.Object)
	 */
	@Override
	public CompressedArray set(int index, CompressedArray element) {

		return map.set(index, element);
	}

	/* (non-Javadoc)
	 * @see java.util.List#add(int, java.lang.Object)
	 */
	@Override
	public void add(int index, CompressedArray element) {
		
		map.add(index, element);
	}
	
	
	/* (non-Javadoc)
	 * @see java.util.List#remove(int)
	 */
	@Override
	public CompressedArray remove(int index) {
		
		return map.remove(index);
	}

	/* (non-Javadoc)
	 * @see java.util.List#indexOf(java.lang.Object)
	 */
	@Override
	public int indexOf(Object o) {
		
		return map.indexOf(o);
	}

	/* (non-Javadoc)
	 * @see java.util.List#lastIndexOf(java.lang.Object)
	 */
	@Override
	public int lastIndexOf(Object o) {
		return map.lastIndexOf(o);
	}

	/* (non-Javadoc)
	 * @see java.util.List#listIterator()
	 */
	@Override
	public ListIterator<CompressedArray> listIterator() {
		
		return map.listIterator();
	}

	/* (non-Javadoc)
	 * @see java.util.List#listIterator(int)
	 */
	@Override
	public ListIterator<CompressedArray> listIterator(int index) {
		
		return map.listIterator(index);
	}

	/* (non-Javadoc)
	 * @see java.util.List#subList(int, int)
	 */
	@Override
	public List<CompressedArray> subList(int fromIndex, int toIndex) {

		return map.subList(fromIndex, toIndex);
	}

	/**
	 * Get the type of cell by coordinate position
	 * 
	 * @param pos coordinate position on the map
	 */
	public TerrainType getByCoordinate(Coordinate pos) {
		return getTerrainTypeByCoordinate(pos.getX(), pos.getY());
	}
	
	/**
	 * Get type of cell by x,y coordinate
	 * 
	 * @param x	x-position in the matrix
	 * @param y	y-position in the mater
	 * @return	the type of the terrain
	 */
	public TerrainType getTerrainTypeByCoordinate(final int x, final int y) {
		CompressedArray row = map.get(y);
		return TerrainType.values()[row.get(x)];
	}
	
	/**
	 * Get integer value by x,y coordinate
	 * 
	 * @param x	x-position in the matrix
	 * @param y	y-position in the mater
	 * @return	the integer value
	 */
	public int getByCoordinate(final int x, final int y) {
		CompressedArray row = map.get(y);
		return row.get(x);
	}
	
	/**
	 * Set the terrain type at given coordinate position
	 * 
	 * @param pos coordinate position on the map 
	 * @param type type of terrain
	 * @return	the type
	 */
	public int setByCoordinate(Coordinate pos, int type) {
		CompressedArray row = map.get(pos.getY());
		return row.set(pos.getX(), type);
	}
	

	/**
	 * Set the terrain type at given x,y position
	 * 
	 * @param x	x-position in the matrix
	 * @param y	y-position in the matrix
	 * @param type	type of terrain
	 * @return	the type
	 */
	public int setByCoordinate(final int x, final int y, final int type) {
		CompressedArray row = map.get(y);
		return row.set(x, type);
	}
	
	/**
	 * @return	number of rows in the matrix
	 */
	public int getNumRows() {
		return map.size();
	}
	
	/**
	 * @return	number of columns in the matrix
	 */
	public int getNumColumns() {
		return map.get(0).size();
	}
	
}
