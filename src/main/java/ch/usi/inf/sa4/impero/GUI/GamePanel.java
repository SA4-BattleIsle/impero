package ch.usi.inf.sa4.impero.GUI;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.GridLayout;
import java.awt.Point;

import javax.swing.BorderFactory;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JToolBar;
import javax.swing.SwingUtilities;

import ch.usi.inf.sa4.impero.game.Game;
import ch.usi.inf.sa4.impero.game.GameObserver;
import ch.usi.inf.sa4.impero.game.PlayerObserver;
import ch.usi.inf.sa4.impero.hexMap.Coordinate;
import ch.usi.inf.sa4.impero.hexMap.HexMap;
import ch.usi.inf.sa4.impero.utility.PixelHexMapConverter;

/**
 * 
 * @author Jesper
 *
 */
public class GamePanel extends JPanel implements GameObserver, PlayerObserver{

	/**
	 * 
	 */
	private static final long serialVersionUID = 5342305996573034262L;
	private final Game game;
	private final MapCanvas canvas;
	private final ArmyPanel armyPanel;
	private final TurnPanel turnPanel;
	private final LogPanel logPanel;
	private final JScrollPane scroller;
	private int width = 800;
	private int height = 600;
	
	public GamePanel(final Game game) {
		super(new BorderLayout());
		game.addObserver(this);
		game.getPlayerByIndex(0).addObserver(this);
		this.game = game;
        // create a sidebar panel to keep turn, army and log
        JPanel sidebar = new JPanel();
        int sidebarWidth = 250;
        sidebar.setBorder(BorderFactory.createEmptyBorder(0, 5, 0, 0));
        sidebar.setLayout(new GridLayout(0, 1, 5, 5));
        this.armyPanel = new ArmyPanel(sidebarWidth, this);
        this.turnPanel = new TurnPanel(sidebarWidth, this);
        this.logPanel = new LogPanel(sidebarWidth);
        sidebar.add(this.turnPanel);
        sidebar.add(this.armyPanel);
        sidebar.add(this.logPanel);
		
		//Set up the map canvas
        this.canvas = new MapCanvas(game, this.logPanel, this);
		
		//set up a toolbar
		JToolBar gameToolbar = new GameToolbar("Game Toolbar", game, this.canvas);
		
        //Put the drawing area in a scroll pane.
        this.scroller = new JScrollPane(this.canvas);
        // set the size of the scroller
        scroller.setPreferredSize(new Dimension(this.width, this.height));
        // disable mouse wheel scrolling
        scroller.setWheelScrollingEnabled(false);
//        scroller.getVerticalScrollBar().setUnitIncrement(40);
//        scroller.getHorizontalScrollBar().setUnitIncrement(40);
        
        //Lay out the toolbar and map.
        this.add(gameToolbar, BorderLayout.PAGE_START);
        this.add(scroller, BorderLayout.CENTER);
        this.add(sidebar, BorderLayout.LINE_END);
        
	}

	/**
	 * @return the game object
	 */
	public Game getGame(){
		return this.game;
	}
	
	/**
	 * @return the canvas
	 */
	public MapCanvas getCanvas() {
		return canvas;
	}

	/**
	 * @return the armyPanel
	 */
	public ArmyPanel getArmyPanel() {
		return armyPanel;
	}

	/**
	 * @return the turnPanel
	 */
	public TurnPanel getTurnPanel() {
		return turnPanel;
	}

	/**
	 * @return the logPanel
	 */
	public LogPanel getLogPanel() {
		return logPanel;
	}
	
	
	public void centerMap() {
		HexMap map = game.getMap();
		Coordinate c = new Coordinate(map.getWidth() / 2, map.getHeight() /2);
		centerMapAtCoordinate(c);
	}
	
	public void centerMapAtCoordinate(final Coordinate c) {
				
		// scroll to the coordinate
        SwingUtilities.invokeLater(new Runnable() {
            public void run() {
            	
        		double x = c.getX();
        		double y = c.getY();
        		double width = game.getMap().getWidth();
        		double height = game.getMap().getHeight();
        		double hMax = scroller.getHorizontalScrollBar().getMaximum();
                double vMax = scroller.getVerticalScrollBar().getMaximum();
                		
        		final int scrollWidth = (int) (x / width * hMax) - scroller.getHorizontalScrollBar().getVisibleAmount()/2;
                final int scrollHeight = (int) (y / height * vMax) - scroller.getVerticalScrollBar().getVisibleAmount()/2;
                
                scroller.getHorizontalScrollBar().setValue(scrollWidth);
                scroller.getVerticalScrollBar().setValue(scrollHeight);
                
                canvas.setClickedCell(c);
            }
          });


	}

	
	
	@Override
	public void refreshMapDisplayRequired() {
		this.canvas.repaint();
	}

	@Override
	public void refreshEntityListDisplayRequired() {
//		System.out.println("Refresh Entity List Required");
		this.armyPanel.repaint();
	}

	@Override
	public void refreshActionListDisplayRequired() {
//		System.out.println("Refresh Turn List Required");
		this.turnPanel.repaint();
	}

	@Override
	public void logEvent(final String text) {
		this.logPanel.appendLogString(text);
	}

	@Override
	public void gameStarted() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void activePlayerChanged() {
		// TODO Auto-generated method stub
//		System.out.println("CHANGE!");
		this.turnPanel.repaint();
		this.armyPanel.repaint();
	}

}
