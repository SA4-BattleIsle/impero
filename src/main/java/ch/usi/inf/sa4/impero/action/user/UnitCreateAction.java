package ch.usi.inf.sa4.impero.action.user;


import ch.usi.inf.sa4.impero.action.cost.UnitCostMapper;
import ch.usi.inf.sa4.impero.entity.AbstractUnit;
import ch.usi.inf.sa4.impero.entity.Entity;
import ch.usi.inf.sa4.impero.game.Game;
import ch.usi.inf.sa4.impero.game.Player;
import ch.usi.inf.sa4.impero.hexMap.Coordinate;
import ch.usi.inf.sa4.impero.hexMap.terrain.TerrainType;

/**
 * This class represent a unit creation action. It need to know the coordinate of the building that the player want to use to create the unit and 
 * the type of the unit.
 * 
 * @author 
 *
 */

public class UnitCreateAction extends AbstractUserAction {
	//coordinate of the building that the player want use to create the unit
	private final Coordinate fromWhat;
	//class of the unit that the player want to create
	private final Class<? extends AbstractUnit> what;
	
	/**
	 * Constructor.
	 * 
	 * @param fromwhat2
	 * @param c
	 */
	public UnitCreateAction(Coordinate fromwhat2, Class<? extends AbstractUnit> c, Player owner) {
		super(owner, fromwhat2);
		this.fromWhat = fromwhat2;
		this.what = c;
	}

	@Override
	public void execute(Game game) {
		game.logEvent("Execute: "+ this.toString());
		Entity unit = null;
		
		try {
			unit = (AbstractUnit) what.getConstructor(this.owner.getClass()).newInstance(this.owner);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} 
		Coordinate spawnCoordinate = game.getMap().findFreeCoordinateAround(fromWhat, what);;
		
		game.addEntity(spawnCoordinate, unit);
		//Refresh of the entity list is required
		game.refreshEntityListDisplayRequired();
		//Refresh of the map is required
		game.refreshMapDisplayRequired();
	}
	
	@Override
	public int getActionPointCost() {
		return UnitCostMapper.getActionPointCost(this.what);
	}

	@Override
	public int getEnergyCost() {
		return UnitCostMapper.getEnergyCost(this.what);
	}

	public String toString(){
		return "Create a " + this.what.getSimpleName();
	}
}
