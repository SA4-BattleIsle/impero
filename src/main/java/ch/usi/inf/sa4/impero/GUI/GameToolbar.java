package ch.usi.inf.sa4.impero.GUI;


import java.awt.Dimension;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.util.List;

import javax.swing.ButtonGroup;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.JSlider;
import javax.swing.JToolBar;
import javax.swing.SwingUtilities;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;

import ch.usi.inf.sa4.impero.entity.AbstractUnit;
import ch.usi.inf.sa4.impero.entity.Entity;
import ch.usi.inf.sa4.impero.entity.HeadQuarter;
import ch.usi.inf.sa4.impero.game.Game;
import ch.usi.inf.sa4.impero.game.GameObserver;
import ch.usi.inf.sa4.impero.game.Player;
import ch.usi.inf.sa4.impero.game.PlayerObserver;

public class GameToolbar extends JToolBar implements ChangeListener, GameObserver, ItemListener, PlayerObserver {
	
	private MapCanvas canvas = null;
	ButtonGroup playersButtonGroup;
	JPanel playersPanel;
	JLabel playerStats;
	Game game;
	
	/**
	 * @author Jesper, Talal
	 * 
	 */
	private static final long serialVersionUID = 765489258651291862L;

	public GameToolbar(final String name, final Game game, final MapCanvas canvas) {
		super(name);
		this.canvas = canvas;
		this.game = game;
		// observe game changes
		game.addObserver(this);
		
		JButton button = null;
		
		// ============= add start game button ====================
		
		button = new JButton("start");
		button.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				
				// start the game
				game.addPlayer(new Player("Michael", "Jackson", game.getActionPointsPerTurn()));
				game.addPlayer(new Player("James", "Bond", game.getActionPointsPerTurn()));
				game.start();
				// disable the button
				JButton b = (JButton) e.getSource();
				b.setEnabled(false);
			}
		});
		this.add(button);		
		
		// ============= add end turn button ====================
		button = new JButton("End turn");
		button.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				game.execute();
				game.beforeTurn();
			}
		});
		this.add(button);
		
		// ============= add player switcher ====================
		
		this.playersPanel = new JPanel();
		playersPanel.setLayout(new GridLayout(1, 0, 5, 0));
		this.playersButtonGroup = new ButtonGroup();
		updatePlayerRadioGroup();
		this.add(this.playersPanel);
		
		// ============= add player status ====================
		
		this.playerStats = new JLabel();
		this.add(this.playerStats);
		this.updatePlayerStats();

		
		// ============= add zoom slider ====================
		
		JSlider zoom = new JSlider(1, 10, 5);
		zoom.addChangeListener(this);
		zoom.setMajorTickSpacing(2);
		zoom.setMinorTickSpacing(1);
		zoom.setPaintTicks(true);
		zoom.setPaintLabels(true);
		zoom.setPreferredSize(new Dimension(100, 40));
		
		this.add(zoom);
		
	}

	/* (non-Javadoc)
	 * @see javax.swing.event.ChangeListener#stateChanged(javax.swing.event.ChangeEvent)
	 */
	@Override
	public void stateChanged(ChangeEvent e) {
		
		JSlider source = (JSlider) e.getSource();
		
		if (!source.getValueIsAdjusting()) {
			canvas.setHexagonRadius(source.getValue() * 5);
			canvas.repaint();
			
			// center the map
			GamePanel g = (GamePanel) this.getParent();
			g.centerMap();
		}
		
	}
	
	/**
	 * 
	 */
	public void updatePlayerRadioGroup() {	
		boolean active;
		JRadioButton player;
		playersPanel.removeAll();
		playersButtonGroup = new ButtonGroup();
		for (Player p : game.getPlayers()) {
			active = (p.equals(game.getActivePlayer()));
			player = new JRadioButton(p.getFirstName(), active);
			// store players hash code to later set the active player in game
			player.setName(""+p.hashCode());
			player.addItemListener(this);
			playersPanel.add(player);
			playersButtonGroup.add(player);
		}
				
	}
	
	public void updatePlayerStats() {
		
		Player p = this.game.getActivePlayer();
		String text = "Points: " + p.getTurn().getActionPoints() + " Energy: " + p.getEnergyLevel();
		this.playerStats.setText(text);
		
	}

	/* (non-Javadoc)
	 * @see ch.usi.inf.sa4.impero.game.GameObserver#refreshMapDisplayRequired()
	 */
	@Override
	public void refreshMapDisplayRequired() {
		
		
	}

	/* (non-Javadoc)
	 * @see ch.usi.inf.sa4.impero.game.GameObserver#refreshEntityListDisplayRequired()
	 */
	@Override
	public void refreshEntityListDisplayRequired() {
		
		
	}


	/* (non-Javadoc)
	 * @see ch.usi.inf.sa4.impero.game.GameObserver#logEvent(java.lang.String)
	 */
	@Override
	public void logEvent(String text) {
		// TODO Auto-generated method stub
		
	}

	/* (non-Javadoc)
	 * @see ch.usi.inf.sa4.impero.game.GameObserver#gameStarted()
	 */
	@Override
	public void gameStarted() {
		this.updatePlayerRadioGroup();
		// add observer to players
		for (Player p : game.getPlayers()) {
			p.addObserver(this);
		}
	}

	/* (non-Javadoc)
	 * @see ch.usi.inf.sa4.impero.game.GameObserver#activePlayerChanged()
	 */
	@Override
	public void activePlayerChanged() {
		
		this.updatePlayerStats();
		game.logEvent("player stats changed");

	}

	/* (non-Javadoc)
	 * @see java.awt.event.ItemListener#itemStateChanged(java.awt.event.ItemEvent)
	 */
	@Override
	public void itemStateChanged(ItemEvent e) {
		JRadioButton b = (JRadioButton) e.getSource();
		if (e.getStateChange() == ItemEvent.SELECTED){
			// set active player according to hash code previously stored as name of radio button
			game.setActivePlayer(Integer.parseInt(b.getName()));
		}
	}

	@Override
	public void refreshActionListDisplayRequired() {
		
		this.updatePlayerStats();
		
	}

}
